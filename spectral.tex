\section{Basic Spectral Theory}
\label{sec:prelim_bst}
In this section we present some baisic spectral theory, both to make the reader
familiar with our notation and to give the reader a quick remainder of the
relevant spectral theory.\\


This overview follows the lecutres \cite{vorlmmqm}, \cite{vorlsth} and the
books \cite{weidlbook}, \cite{birsol} and \cite{reedsimon} from whom the author
has taken the liberty to (re)arrange and mix as he saw fit.
For the reader who does not have access to these lectures it should be noted
that the content should be covered by most lectures and literature on the
subject, for example by the latter two books \cite{reedsimon}, \cite{birsol}.
This chapter does not contain any ideas by the author.
\begin{defn}
  Let $H$ be a Hilbertspace and
  let $A:D(A)\subset H\to H$ be a (potentially unbounded) closed linear operator.
  Then we define the
  \begin{enumerate}
    \item
    Resolvent $R(z) := (A-z)^{-1}$,
    \item
    Resolventset $z\in \rho(A)\gdw \exists R(z)\in B(H)$,
    \item
    Spectrum $\sigma(A):=\C\sm\rho(A)$,
    \item
    Point spectrum $\sigma_p(A):=\mdef{z\in\C}{\ker(A-z)\neq\m{0}}$,
    \item
    Continous spectrum $\sigma_c(A):=\mdef{z\in\C}{\Im(A-z)\neq \overline{\Im(A-z)}}$, and
    \item
    Residual spectrum $\sigma_r(A):=\sigma\sm\m{\sigma_p(A)\cup\sigma_c(A)}$.
  \end{enumerate}
\end{defn}
\begin{thm}
  If $A$ is self-adjoint, then $\sigma(A)\subset\R$.
\end{thm}
\begin{defn}[Projection valued measure]
  Let $\sB$ be the $\sigma$-Algebra of Borel measurable sets on $\R$. Then
  $P: \sB \to B(H), \omega \mapsto P_\omega$ is called a projection valued measure iff
  \begin{enumerate}
    \item $P_\omega$ is an orthogonal projection ($P_\omega=P_\omega^*=P_\omega^2$)
    \item $P_\lm = 0$, $P_\R=1$
    \item $\omega = \cup_{n\in\N}\omega_n \land \fa{n\neq m\in\N} \omega_n\cap\omega_m=\lm
            \f P_\omega=\slim_{N\to\infty}\sum_{n=1}^N P_{\omega_n}$
    \item $P_{\omega_1}P_{\omega_2}=P_{\omega_1\cap\omega_2}$
  \end{enumerate}
  The support of $P$ is defined as $z\in \supp P \gdw \fal{\text{Neighbourhoods $U$ of z}}P_U \neq 0$.
\end{defn}
\begin{cor}
  A projection valued measure defines a non-negative measure $\mu_f$ for each $f\in H$ by
  \eq{\mu_f(\omega)=(P_\omega f, f)}
  and a complex measure for each pair $f,g\in H$ by
  \eq{\mu_{f,g} = (P_\omega f, g).}
  Alternatively the latter can also be defined by polarization with use of $\mu_{f\pm g}$, $\mu_{f\pm ig}$.
\end{cor}
\begin{thm} \label{pvmdop}
  Let $P$ be a projection valued measure.
  Let \eq{D(A) := \mdef{f\in H}{\int \lambda^2 \d\mu_{f,f}(\lambda)\le\infty}.}
  Then by the Riesz representation theorem for each $f\in D(A)$ there exists a $w\in H$ such that
  \eq{\fa{g\in H} (w, g) = \int \lambda \d\mu_{f,g}(\lambda).}
  We use this to define a self-adjoint operator $A: D(A) \to H$ by $f\mapsto Af := w$.
  We write $A = \int \lambda \d P(\lambda)$.
\end{thm}
\begin{thm}[Spectral theorem]
  For every self-adjoint operator $A$ there exists a spectral measure $P$
  such that $A = \int \lambda \d P(\lambda)$.
  $P$ is then called the spectral measure of $A$ and we denote this measure by $P=P(A)$.
\end{thm}
\begin{thm}
  It holds that $\supp P = \sigma(A)$.
\end{thm}
\begin{cor}[Functional calculus]
  Let $\phi$ be a bounded Borel measurable function.
  Let $A$ be a self-adjoint operator and let $P$ be its spectral measure.
  Then we can define $\phi(A):= \int \phi \d P$ in a similar fashion to Thm. \ref{pvmdop}:
  \eq{\fa{g\in H} (\phi(A)f, g) = \int \phi(\lambda) \d\mu_{f,g}(\lambda)}
  for each $f$ in the domain of $\phi(A)$ which we define as
  \eq{D(\phi(A)) := \mdef{f\in H}{\int \abs{\phi(\lambda)}^2 \d\mu_{f,f}(\lambda)\le\infty}.}
\end{cor}
\begin{thm}[Properties of the functional calculus]\,
  \begin{enumerate}
    \item For $f\in D(A)$ have the identity
      \eq{\norm{\phi(A)f}^2 = \int \abs{\phi(\lambda)}^2\d\mu_{f,f}(\lambda).}
    \item 
      This functional calculus is a *-homomorphism:
      \begin{enumerate}
      \item $(\alpha\phi_1 + \beta\phi_2)(A) \supset \alpha\phi_1(A) + \beta\phi_2(A)$
      \item $(\phi_1\cdot\phi_2)(A) \supset \phi_1(A)\phi_2(A)$
      \item $(\bar\phi)(A) = \phi(A)^*$
      \end{enumerate}
    \item $(\phi_2\circ\phi_1)(A)=\phi_2(\phi_1(A))$
    \item $\phi$ fulfills the Spectral mapping theorem: $P_\omega(\phi(A))=P_{\phi^{-1}(\omega)}(A)$
    \item $\sigma(\phi(A)) = \overline{\phi(\sigma(A))}$
  \end{enumerate}
\end{thm}
\begin{defn}
  We define $A_+ := \chi(\R_{> 0})(A)$ and $A_- := -\chi(\R_{\le 0})(A)$.
\end{defn}
\begin{defn}
We define
  \begin{enumerate}
    \item 
the discrete spectrum
$\sigma_d(A) := \mdef{z\in\sigma(A)}{\text{$z$ is an isolated point in $\sigma(A)$
  and $\dim P_{\m{z}} < \infty$}}$ and
    \item
the essential spectrum $\sigma_{ess}(A):=\sigma(A)\sm\sigma_d(A)$.
  \end{enumerate}
\end{defn}
\begin{defn}
  A symmetric operator $A$ is called lower semibounded with constant $m_A$ iff
  \eq{m_A := \inf_{u\in D(A)} \frac{(Au,u)}{\norm{u}^2} > -\infty.}
\end{defn}
\begin{thm}
  For a self-adjoint operator $A$ it holds that
  \eq{m_A = \inf \sigma(A).}
\end{thm}
\begin{defn}
  A quadratic form $a$ is called associated with the operator $A$ if
  \eq{D(a)\supset D(A)}
  and
  \eq{a[x,y] = (Ax,y)}
  holds for all $x\in D(A)$, $y\in D(a)$.
\end{defn}
\begin{thm}\label{saoplsquadform}
  Let $a$ be a lower semibounded, closed quadratic form.
  Then there exists a unique self-adjoint Operator $A$ that is associated with
  $a$. The domain of $A$ is given by
  \eq{D(A) = \mdef{u\in D(a)}{\ex{f\in H}\fa{v\in D(a)} a[u,v] = (f,v)}.}
\end{thm}
