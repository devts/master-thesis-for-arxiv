\section{Theory for 3 power interpolation}\label{intp3}
\subsection{Central definitions and theorems}\label{intp3gen}
Now we try to extend the formalism presented above to use results for 3 exponent values $p_0$, $p_1$, $p_2$ at once.
\begin{defn}[$K$-function]\label{defK3}
  For $(u_n)_{n\in\N}\in l_{p_0}+l_{p_1}+l_{p_2}$ and $t,s > 0$ we define
  \eq{K((u_n), t, s, p_0, p_1, p_2) := \inf_{u_n=u_n^{(0)}+u_n^{(1)}+u_n^{(2)}, u_n^{(i)}\in l_{p_i}}
      \left(\norm{u_n^{(0)}}^{p_0}_{l_0}+t\norm{u_n^{(1)}}^{p_1}_{l_1}+s\norm{u_n^{(2)}}^{p_2}_{l_2}\right)}
  and similarily for $L_p$-spaces we define for $f\in L_{p_0}+L_{p_1}+L_{p_2}$ and $t,s>0$
  \eq{K(f, t, s, p_0, p_1, p_2) := \inf_{f=f_0+f_1+f_2, f_i\in L_{p_i}}
      \left(\norm{f_0}^{p_0}_{L_{p_0}}+t\norm{f_1}^{p_1}_{L_{p_1}}+s\norm{f_2}^{p_2}_{L_{p_2}}\right).}
\end{defn}
\begin{defn}[Interpolation functional]
  For $\eta, \xi\in (0,1)$, $\eta + \xi < 1$ and $q > 0$ we define the functional $\Phi_{g,q}$
  on the functions $h:(0,\infty)\times (0,\infty)\to [0,\infty )$ with $g(t,s)=g_{\eta,\xi}(t,s) = t^{-\eta-1}s^{-\xi-1}$:
  \eq{\Phi_{g,q}[h]:=\begin{cases}\sds\left(\int_0^\infty\int_0^\infty
    (ts g(t,s)h(t, s))^q t^{-1} s^{-1}\d t\d s\right)^\frac{1}{q} & q < \infty\\
  \sds\sup_{t, s > 0} ts g(t,s)h(t, s) & q = \infty\end{cases}}
\end{defn}
\begin{thm}[Monotonicity]\label{thmfineq3}
  Let $h_1,h_2:(0,\infty)\to [0,\infty )$, then
  $h_1\le h_1$ implies $\Phi_{g,q}[h_1] \le \Phi_{g,q}[h_2]$.
\end{thm}
\begin{thm}\label{eta1eq3}
  Let $0 < p_0 < p_1 < p_2 < \infty$,  $\eta,\xi \in (0,1)$, $\eta + \xi < 1$, then
  \eq{\Phi_{g,1}[K((u_n), t, s, p_0, p_1, p_2)] = \Theta(\eta, \xi, p_0, p_1, p_2)\norm{u_n}^p_{l_p}.}
  with
  \eq{p=p_0+\eta(p_1-p_0)+\xi(p_2-p_0)}
  and
  \eq{\Theta(\eta, \xi, p_0, p_1, p_2) = \int_0^\infty \int_0^\infty g_{\eta,\xi}(t,s) F(t, s, p_0, p_1, p_2)\d t\d s}
  where \eq{F(t, s, p_0, p_1, p_2) = \inf_{y_0+y_1+y_2=1}\left(\abs{y_0}^{p_0}+t\abs{y_1}^{p_1}+s\abs{y_2}^{p_2}\right).}
  \begin{proof}
    In the following we follow the proof in \cite[Thm 5.2.2 (p.111)]{intspbergh}.
    We prove the theorem for $f\in L_p(X,\mu)$, the case for $(u_n)\in l_p$ can then be regarded as special case $L_p(\N,\#)$.
    Then it follows
    \eq{K(f, t, s, p_0, p_1, p_2) &= \inf_{f=f_0+f_1+f_2} \int_X \abs{f_0}^{p_0}+t\abs{f_1}^{p_1}+s\abs{f_2}^{p_2}\d\mu \\
        &= \int_X\inf_{f(x)=f_0(x)+f_1(x)+f_2(x)}\abs{f_0(x)}^{p_0}+t\abs{f_1(x)}^{p_1}+s\abs{f_2(x)}^{p_2}\d\mu \\
        &= \int_X\abs{f(x)}^{p_0}\inf_{f(x)=f_0(x)+f_1(x)+f_2(x)}
              \abs{\frac{f_0(x)}{f(x)}}^{p_0}+t\abs{f(x)}^{p_1-p_0}\abs{\frac{f_1(x)}{f(x)}}^{p_1}\\&\quad\quad
               +s\abs{f(x)}^{p_2-p_0}\abs{\frac{f_2(x)}{f(x)}}^{p_2}\d\mu \\
        &= \int_X\abs{f(x)}^{p_0}F(t\abs{f(x)}^{p_1-p_0}, s\abs{f(x)}^{p_2-p_0}, p_0, p_1, p_2)\d\mu \quad\text{with $y_i=\frac{f_i(x)}{f(x)}$.}\\
    }
    By applying $\Phi_{g,1}$ we yield the desired equality:
    \eq{\Phi_{\eta,1}[K(f, t, p_0, p_1)] &=\int_0^\infty \int_0^\infty g(t,s)\int_X\abs{f(x)}^{p_0}
           F(t\abs{f(x)}^{p_1-p_0}, s\abs{f(x)}^{p_2-p_0}, p_0, p_1, p_2)\d\mu\d t\d s\\
         &= \int_X\abs{f(x)}^{p_0}\int_0^\infty\int_0^\infty g(t,s)
           F(t\abs{f(x)}^{p_1-p_0}, s\abs{f(x)}^{p_2-p_0}, p_0, p_1, p_2)\d t\d s\d\mu\\
         &\,\text{Subst. $t'=t\abs{f(x)}^{p_1-p_0}$, $s'=s\abs{f(x)}^{p_2-p_0}$}\\
         &= \int_X\abs{f(x)}^{p_0}\int_0^\infty g\left(\frac{t'}{\abs{f(x)}^{p_1-p_0}}, \frac{s'}{\abs{f(x)}^{p_2-p_0}}\right)
           \frac{F(t', s', p_0, p_1, p_2)}{\abs{f(x)}^{p_1-p_0}\abs{f(x)}^{p_2-p_0}}\d s'\d t'\d\mu\\
         &= \int_X\abs{f(x)}^{p_0+\eta(p_1-p_0)+\xi(p_2-p_0)}\int_0^\infty g(t',s')F(t, s, p_0, p_1, p_2)\d s'\d t'\d\mu\\
         &= \Theta(\eta, \xi, p_0, p_1, p_2)\int_X\abs{f(x)}^p\d\mu.
    }
  \end{proof}
\end{thm}
\begin{thm}
  $F$ as defined in Thm. \ref{eta1eq3} fulfills
  \eq{F(t, s, p_0, p_1, p_2) \le \min(t,s,1).}
  \begin{proof}
    The edge cases in the infimum $(y_0,y_1,y_2)\in\m{(1,0,0), (0,1,0), (0,0,1)}$ yield this estimate.
  \end{proof}
\end{thm}
\begin{cor}
  $\Theta$ as defined in Thm. \ref{eta1eq3} is well defined.
  \begin{proof}
    We define $\delta := \frac{\eta}{\eta+\xi}$.
    Then it holds that
    \eq{
      -\eta-1+\delta = -1 + \eta\Bigg(\ubr{\frac{1}{\eta+\xi}}{>1}-1\Bigg)>-1,
      \qquad -\xi-\delta = -1 + \xi\Bigg(\ubr{\frac{1}{\eta+\xi}}{>1}-1\Bigg)>-1}
    and \eq{F(t, s, p_0, p_1, p_2)\le \min(t,s,1)\le \begin{cases}t^\delta s^{1-\delta}\\ t^\delta \\ s^{1-\delta}\end{cases}.}
    So it follows that \eq{
      \Theta(\eta, \xi, p_0, p_1, p_2) &= \int_0^\infty \int_0^\infty t^{-\eta-1}s^{-\xi-1} F(t, s, p_0, p_1, p_2)\d t\d s\\
      &= \int_0^1 t^{-\eta-1+\delta} \int_0^1 s^{-\xi-\delta} \ubr{\frac{F(t,s,p_0,p_1,p_2)}{t^\delta s^{1-\delta}}}{\le 1}\d s \d t\\
      &\quad+ \int_1^\infty t^{-\eta-1} \int_1^\infty s^{-\xi-1} \ubr{F(t,s,p_0,p_1,p_2)}{\le 1}\d s \d t\\
      &\quad+ \int_1^\infty t^{-\eta-1} \int_0^1 s^{-\xi-\delta} \ubr{\frac{F(t,s,p_0,p_1,p_2)}{s^{1-\delta}}}{\le 1}\d s \d t\\
      &\quad+ \int_0^1 t^{-\eta-1+\delta} \int_1^\infty s^{-\xi-1} \ubr{\frac{F(t,s,p_0,p_1,p_2)}{t^\delta}}{\le 1}\d s \d t < \infty
    }
  \end{proof}
\end{cor}
\begin{cor}\label{etachoice}
  If we want an interpolation result for $L_{p,d}$, then $\xi$ can be related to $\eta$ by
  \eq{\xi = \frac{p - p_0}{p_2 - p_0} - \eta \frac{p_1 - p_0}{p_2 - p_0}.}
  But this leaves $\eta$ fee to vary between certain bounds:\\
  Since $\xi(\eta) > 0$ it must hold that $\eta < \frac{p - p_0}{p_1 - p_0}$.\\
  Since $\eta + \xi(\eta) < 1$ it must hold that $\eta < \frac{p_2 - p}{p_2 - p_1}$.\\
  Which ultimately yields that $\eta$ can be chosen in the following bounds:
  \eq{0<\eta<\eta_\text{max}:=\begin{cases}\frac{p - p_0}{p_1 - p_0} & p \le p_1 \\ \frac{p_2 - p}{p_2 - p_1} & p \ge p_1\end{cases}}
\end{cor}
\begin{thm}\label{Phitsfactor}
  For $h(t)=\tilde{h}(\alpha t, \beta s)$ it holds in general that
  \eq{\Phi_{g,q}[h] = \alpha^\eta\beta^\xi \Phi_{g,q}[\tilde{h}]}
  \begin{proof}
    This can be proven by a quick calculation:
    \eq{\Phi_{g,q}[h] \overset{q\neq\infty}&{=}
        \left(\int_0^\infty\int_0^\infty (t^{-\eta}s^{-\xi}\tilde{h}(\alpha t, \beta s))^qt^{-1}s^{-1}\d t\d s\right)^\frac{1}{q}
        & \text{Subst. $t' = \alpha t$, $s' = \beta s$}\\
        &= \left(\int_0^\infty\int_0^\infty (\alpha^\eta\beta^\xi {t'}^{-\eta}{s'}^{-\xi}
           \tilde{h}(t', s'))^qt'^{-1}s'^{-1}\d t'\d s'\right)^\frac{1}{q} \\
        &= \alpha^\eta\beta^\xi\Phi_{g,q}[\tilde{h}]
    }
    and the same substitution can also be used to proof the case $q=\infty$.
  \end{proof}
\end{thm}
\begin{thm}\label{phig1linear}
  $\Phi_{g, 1}$ is a linear functional.
  \begin{proof}
    This can be proven by a quick calculation:
    \eq{
      \Phi_{g, 1}[h_0+\alpha h_1] &= \int_0^\infty g(t,s)(h_0(t) + \alpha h_1(t))\d t \\
          &= \int_0^\infty g(t,s)h_0(t)\d t + \alpha \int_0^\infty g(t,s)h_1(t)\d t \\
          &=\Phi_{g, 1}[h_0]+\alpha\Phi_{g, 1}[h_1].
    }
  \end{proof}
\end{thm}
\begin{defn}[Decomposition of $H$] \label{zerlH3}
  Let $H=-\laplace + V$, $V\le 0$, $V\in L_p(\R^d)$, $V=V_0+V_1+V_2$, $L_p(\R^d)\ni V_0, V_1, V_2\le 0$,
  $\theta_0,\theta_1\in (0,1)$, $\theta_0+\theta_1 < 1$
  with $p$ in the range defined in Def. \ref{defSchroedinger}.
  Then we define $H_0,H_1,H_2$ by the following decomposition of $H$:
  \eq{H = \ubr{-\theta_0\laplace + V_0}{=:H_0} + \ubr{-\theta_1\laplace + V_1}{=:H_1}
      + \ubr{-(1-(\theta_0+\theta_1))\laplace + V_2}{=:H_2}.}
  where all operators are formally defined by a quadratic form as in Def. \ref{defSchroedinger}.
  The domain of all three operators is therefore $H^1(\R^d)$.
\end{defn}
\begin{rem}
  For $V\le 0$ we have
  \eq{K(V, t, s, p_0, p_1, p_2)
    &= \inf_{V=V_0+V_1, V_i\in L_{p_i}}
      \left(\norm{V_0}^{p_0}_{L_{p_0}}+t\norm{V_1}^{p_1}_{L_{p_1}}+s\norm{V_2}^{p_2}_{L_{p_2}}\right)\\
    &= \inf_{V=V_0+V_1, V_i\in L_{p_i}, V_i\le 0}
      \left(\norm{V_0}^{p_0}_{L_{p_0}}+t\norm{V_1}^{p_1}_{L_{p_1}}+s\norm{V_2}^{p_2}_{L_{p_2}}\right).
  }
  We will use this equality without mentioning it in the following text since
  it is bothersome to carry the large subscript and add an extra step for the
  formality.
\end{rem}
\begin{thm}\label{abschEHi3}
  For the decompositions we have the following Lieb-Thirring type inequalites:
  \eq{
    \norm{E_k(H_0)}_{l_{p_0}}^{p_0} = \sum_k \abs{E_k(H_0)}^{p_0}
    \le \theta_0^{-\kappa} L_{p_0,d}\norm{V_0}_{l_{p_0+\kappa}}^{p_0+\kappa}
  }
  and
  \eq{
    \norm{E_k(H_1)}_{l_{p_1}}^{p_1} = \sum_k \abs{E_k(H_1)}^{p_1}
    \le \theta_1^{-\kappa} L_{p_1,d}\norm{V_1}_{l_{p_1+\kappa}}^{p_1+\kappa}
  }
  and
  \eq{
    \norm{E_k(H_2)}_{l_{p_2}}^{p_2} = \sum_k \abs{E_k(H_2)}^{p_2}
    \le (1-(\theta_0+\theta_1))^{-\kappa} L_{p_2,d}\norm{V_1}_{l_{p_2+\kappa}}^{p_2+\kappa}.
  }
  \begin{proof}
    Similar to Thm \ref{abschEHi}.
  \end{proof}
\end{thm}
