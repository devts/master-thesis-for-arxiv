\section{Classic Interpolation theory}\label{intp}
In \ref{intp2gen} and \ref{intp2kf} we present the interpolation theory given
in \cite{weidlinterpolation} in slightly more detailed and general fashion.
In particular we will derive a slightly more generalized approximation in that
we provide an approximation for $L_{\gamma,d}$ instead of $L_{\gamma, 1}$,
which in principle allows for interpolation in higher dimensions.
Furthermore we present some numerical computations with updated numbers from
newer results.\\

In \ref{intp2rf} we present some new estimates using Rotfeld inequalities
instead of Ky-fan inequalities. 
This new method leads to significantly improved estimates.
This method also allows us to derive an approximation for $L_{\gamma,d}$
instead of $L_{\gamma, 1}$ and allows us to derive an operator valued adaption
and thus allows us in higher dimension to choose between lifted results and
interpolation in that dimension itself.

\subsection{Central definitions and theorems}\label{intp2gen}
\begin{defn}[$K$-function, \cite{intspbergh}, \cite{weidlinterpolation}]\label{defK}
  For $(u_n)_{n\in\N}\in l_{p_0}+l_{p_1}$ and $t > 0$ we define
  \eq{K((u_n), t, p_0, p_1) := \inf_{u_n=u_n^{(0)}+u_n^{(1)}, u_n^{(i)}\in l_{p_i}}
      \left(\norm{u_n^{(0)}}^{p_0}_{l_{p_0}}+t\norm{u_n^{(1)}}^{p_1}_{l_{p_1}}\right)}
  and similarily for $L_p$-spaces we define for $f\in L_{p_0}+L_{p_1}$ and $t>0$
  \eq{K(f, t, p_0, p_1) := \inf_{f=f_0+f_1, f_i\in L_{p_i}}
      \left(\norm{f_0}^{p_0}_{L_{p_0}}+t\norm{f_1}^{p_1}_{L_{p_1}}\right).}
\end{defn}
\begin{defn}[Interpolation functional, \cite{intspbergh}, \cite{weidlinterpolation}]
  For $\eta\in (0,1)$ and $q > 0$ we define the functional $\Phi_{\eta,q}$
  on the functions $h:(0,\infty)\to [0,\infty )$:
  \eq{\Phi_{\eta,q}[h]:=\begin{cases}\sds\left(\int_0^\infty (t^{-\eta}h(t))^qt^{-1}\d t\right)^\frac{1}{q} & q < \infty\\
  \sds\sup_{t > 0} t^{-\eta}h(t) & q = \infty\end{cases}}
\end{defn}
\begin{thm}[Monotonicty]\label{thmfineq}
  Let $h_1,h_2:(0,\infty)\to [0,\infty )$, then
  $h_1\le h_2$ implies $\Phi_{\eta,q}[h_1] \le \Phi_{\eta,q}[h_2]$.
\end{thm}
\begin{thm}[Interpolation Theorem, \cite{intspbergh}, \cite{weidlinterpolation}]\label{eta1eq}
  Let $p_0 > 0$, $p_1 < \infty$ with $p_1\neq p_0$, $\eta\in (0,1)$, then
  \eq{\Phi_{\eta,1}[K(f, t, p_0, p_1)] = \Theta(\eta, p_0, p_1)\norm{f}^p_{L_p}}
  and
  \eq{\Phi_{\eta,1}[K((u_n), t, p_0, p_1)] = \Theta(\eta, p_0, p_1)\norm{u_n}^p_{l_p}}
  with
  \eq{p=(1-\eta)p_0 + \eta p_1}
  and
  \eq{\Theta(\eta, p_0, p_1)
    := \int_0^\infty t^{-\eta -1} \inf_{y_0+y_1=1} \left(\abs{y_0}^{p_0}+t\abs{y_1}^{p_1}\right) \d t
    = \int_0^\infty t^{-\eta -1} F(t, p_0, p_1) \d t
  }
  where we set \eq{F(t, p_0, p_1) := \inf_{y_0+y_1=1}\left(\abs{y_0}^{p_0}+t\abs{y_1}^{p_1}\right)}
  for convenience.
  \begin{proof}
    In the following we follow the proof in \cite[Thm 5.2.2 (p.111)]{intspbergh}.
    We prove the theorem for $f\in L_p(X,\mu)$, the case for $(u_n)\in l_p$ can then be regarded as special case $L_p(\N,\#)$
    (where $\#$ is the counting measure).
    Then it follows
    \eq{K(f, t, p_0, p_1) &= \inf_{f=f_0+f_1} \int_X \abs{f_0}^{p_0}+t\abs{f_1}^{p_1}\d\mu \\
        &= \int_X\inf_{f(x)=f_0(x)+f_1(x)}\abs{f_0(x)}^{p_0}+t\abs{f_1(x)}^{p_1}\d\mu \\
        &= \int_X\abs{f(x)}^{p_0}\inf_{f(x)=f_0(x)+f_1(x)}
              \abs{\frac{f_0(x)}{f(x)}}^{p_0}+t\abs{f(x)}^{p_1-p_0}\abs{\frac{f_1(x)}{f(x)}}^{p_1}\d\mu \\
        &= \int_X\abs{f(x)}^{p_0}F(t\abs{f(x)}^{p_1-p_0}, p_0, p_1)\d\mu \quad\text{with $y_i=\frac{f_i(x)}{f(x)}$.}
    }
    By applying $\Phi_{\eta,1}$ we yield the desired equality:
    \eq{\Phi_{\eta,1}[K(f, t, p_0, p_1)] &=\int_0^\infty t^{-\eta-1}\int_X\abs{f(x)}^{p_0}F(t\abs{f(x)}^{p_1-p_0}, p_0, p_1)\d\mu\d t\\
         &= \int_X\abs{f(x)}^{p_0}\int_0^\infty t^{-\eta-1}F(t\abs{f(x)}^{p_1-p_0}, p_0, p_1)\d t\d\mu
         \quad \text{Subst. $s=t\abs{f(x)}^{p_1-p_0}$}\\
         &= \int_X\abs{f(x)}^{(1-\eta)p_0+\eta p_1}\int_0^\infty s^{-\eta-1}F(s, p_0, p_1)\d s\d\mu\\
         &= \Theta(\eta, p_0, p_1)\int_X\abs{f(x)}^p\d\mu.
    }
  \end{proof}
\end{thm}
\begin{thm}
  $F$ as defined in Thm. \ref{eta1eq} fulfills
  \eq{F(t, p_0, p_1, p_2) \le \min(t,1).\label{Fbounds}}
  \begin{proof}
    The edge cases in the infimum $(y_0,y_1)\in\m{(1,0), (0,1)}$ yield this estimate.
  \end{proof}
\end{thm}
\begin{cor}
  $\Theta$ as defined in Thm. \ref{eta1eq} is well defined.
  \begin{proof}
  This follows from the bounds we have just proven on $F$ in \eqref{Fbounds}:
    \eq{
      \Theta(\eta, p_0, p_1) &= \int_0^\infty t^{-\eta-1} F(t, p_0, p_1)\d t\\
      &= \int_0^1 t^{-\eta} \ubr{\frac{F(t, p_0, p_1)}{t}}{\le 1}\d t
      + \int_1^\infty t^{-\eta-1} \ubr{F(t, p_0, p_1)}{\le 1}\d t\\
      &\le \left[ \frac{1}{-\eta+1}t^{-\eta+1} \right]_0^1
         + \left[ \frac{1}{-\eta}t^{-\eta} \right]_1^\infty \\
      &= \frac{1}{1-\eta} + \frac{1}{\eta} = \frac{1}{\eta(1-\eta)} < \infty.
    }
  \end{proof}
  It is interesting to note that in the case of $p_0=\frac{1}{2}$, $p_1=1$ we
  achieve equality with the bound derived here.
  For a proof of this, see Cor. \ref{formspltheta}.
\end{cor}
\begin{thm}\label{Phitfactor}
  For $h(t)=\tilde{h}(\alpha t)$ it holds in general that
  \eq{\Phi_{\eta,q}[h] = \alpha^\eta \Phi_{\eta,q}[\tilde{h}].}
  \begin{proof}
    This can be proven by a quick calculation:
    \eq{\Phi_{\eta,q}[h] \overset{q\neq\infty}&{=}
                           \left(\int_0^\infty (t^{-\eta}\tilde{h}(\alpha t))^qt^{-1}\d t\right)^\frac{1}{q}
                           & \text{Subst. $t' = \alpha t$}\\
                         &= \left(\int_0^\infty (\alpha^\eta {t'}^{-\eta}\tilde{h}(t'))^q\frac{\alpha^{-1} \d t'}{\alpha^{-1}t'}\right)^\frac{1}{q} \\
                         &= \alpha^\eta \Phi_{\eta,q}[\tilde{h}]
    }
    and the same Substitution can also be used to proof the case $q=\infty$.
  \end{proof}
\end{thm}
\begin{thm}[Linearity]\label{phi1linear}
  $\Phi_{\eta, 1}$ is a linear functional
  \begin{proof}
    This can be proven by a quick calculation:
    \eq{
      \Phi_{\eta, 1}[h+\alpha g] &= \int_0^\infty t^{-\eta-1}(h(t) + \alpha g(t))\d t \\
          &= \int_0^\infty t^{-\eta-1}h(t)\d t + \alpha \int_0^\infty t^{-\eta-1}g(t)\d t \\
          &=\Phi_{\eta, 1}[h]+\alpha\Phi_{\eta, 1}[g].
    }
  \end{proof}
\end{thm}
\begin{defn}[Decomposition of $H$] \label{zerlH}
  Let $H=-\laplace + V$, $V\le 0$, $V\in L_p(\R^d)$, $V=V_0+V_1$, $L_p(\R^d)\ni V_0,V_1\le 0$,
  $\theta\in (0,1)$ with $p$ in the range defined in Def. \ref{defSchroedinger}.
  Then we define
  \eq{H_0 := -\theta\laplace + V_0,}
  \eq{H_1 := -(1-\theta)\laplace + V_1}
  so that they form the following decomposition of $H$:
  \eq{H = \ubr{-\theta\laplace + V_0}{=H_0} + \ubr{-(1-\theta)\laplace + V_1}{=H_1} = H_0 + H_1.}
  Here all operators are formally defined by a quadratic form as in Def. \ref{defSchroedinger}.
  The quadratic form domain of all three operators is therefore $H^1(\R^d)$.
\end{defn}
\begin{rem}
  For $V\le 0$ we have
  \eq{K(V, t, p_0, p_1)
    &= \inf_{V=V_0+V_1, V_i\in L_{p_i}}
      \left(\norm{V_0}^{p_0}_{L_{p_0}}+t\norm{V_1}^{p_1}_{L_{p_1}}\right)\\
    &= \inf_{V=V_0+V_1, V_i\in L_{p_i}, V_i\le 0}
      \left(\norm{V_0}^{p_0}_{L_{p_0}}+t\norm{V_1}^{p_1}_{L_{p_1}}\right).
  }
  We will use this equality without mentioning it in the following text since
  it is bothersome to carry the large subscript and add an extra step for the
  formality.
\end{rem}
\begin{thm}\label{abschEHi}
  For the decompositions we have the following Lieb-Thirring type inequalities:
  \eq{
    \norm{E_k(H_0)}_{l_{p_0}}^{p_0} = \sum_k \abs{E_k(H_0)}^{p_0}
    \le \theta^{-\kappa} L_{p_0,d}\norm{V_0}_{L_{p_0+\kappa}}^{p_0+\kappa}
  }
  and
  \eq{
    \norm{E_k(H_1)}_{l_{p_1}}^{p_1} = \sum_k \abs{E_k(H_1)}^{p_1}
    \le (1-\theta)^{-\kappa} L_{p_1,d}\norm{V_1}_{L_{p_1+\kappa}}^{p_1+\kappa}.
  }
  \begin{proof}
    Through factoring out the $\theta$ prefactor on the Laplace operator we can
    apply the ordinary Lieb-Thirring inequality:
    \eq{
      \sum_k \abs{E_k(H_0)}^{p_0}
      &= \sum_k \theta^{p_0}\abs{E_k\left(-\laplace + \frac{V_0}{\theta}\right)}^{p_0} \\
      \overset{\eqref{LTeq}}&{\le} \theta^{p_0}L_{p_0,d}\norm{\frac{V_0}{\theta}}_{L_{p_0+\kappa}}^{p_0+\kappa}\\
      &= \theta^{p_0} L_{p_0,d}\theta^{-(p_0+\kappa)}\norm{V_0}_{L_{p_0+\kappa}}^{p_0+\kappa} \\
      &= \theta^{-\kappa} L_{p_0,d}\norm{V_0}_{L_{p_0+\kappa}}^{p_0+\kappa}
    }
    and similar for $H_1$.
  \end{proof}
\end{thm}
\subsubsection{Some explicit formulas for $\Theta$ with exponents of special interest}
\begin{cor}\label{formspltheta}
  For some special exponents closed formulas or simplified expressions can be found:
  \eq{\Theta(\eta, 1, 2) = \frac{2^\eta}{(1-\eta)\eta(\eta+1)},}
  \eq{\Theta\left(\eta, \frac{1}{2}, \frac{3}{2}\right) &= 
        \frac{\left(\frac{2}{3}\sqrt{1+\frac{2}{\sqrt{3}}}\right)^{1-\eta}}{1-\eta}
        + \frac{1}{\sqrt{2}}\left(\frac{3}{2}\right)^\eta\int_{\sqrt{3}-1}^1 w (1-w)^{\frac{\eta}{2}-1}(1+w)^{\frac{\eta}{2}-\frac{1}{2}}\d w\\&\quad
        + \frac{1}{3\sqrt{2}}\left(\frac{3}{2}\right)^\eta\int_{\sqrt{3}-1}^1 w (1-w)^{\frac{\eta}{2}}(1+w)^{\frac{\eta}{2}-\frac{3}{2}}\d w,
  }
  which have already both been given without proof by Weidl in
  \cite{weidlinterpolation}, although we note that our expression for the latter
  term differs by a factor of 2 in the last summand.
  In addition, we derive
  \eq{\Theta\left(\eta, \frac{1}{2}, 1\right) =\frac{1}{\eta(1-\eta)},}
  \eq{\Theta(\eta, 1, \frac{3}{2}) = \frac{3^\eta 2^{1-\eta}}{\eta(1-\eta)(2+\eta)},}
  and
  \eq{F\left(t, \frac{3}{2}, 2\right)
  = \left(\frac{9}{32t^2}\left(1 - \sqrt{1+\frac{64t^2}{9}}\right) + 1\right)^{\frac{3}{2}}
  + t\cdot\left(\frac{9}{32t^2}\left(1 - \sqrt{1+\frac{64t^2}{9}}\right)\right)^2}
  where $F$ is defined as in Thm. \ref{eta1eq} and yields the corresponding
  value for $\Theta$ with an integral as described in Thm. \ref{eta1eq}.
\begin{proof}
  To calculate $\Theta$, we need to calculate the value of the infimum in the
  definition of $F$. So on the calculation of $F$ we introduce
  \eq{y_0=y, y_1=1-y}
  so that the check for local extrema in the calculation of the infimum in $F$ becomes
  \eq{\frac{\d}{\d y}(y^{p_0}+t(1-y)^{p_1}) = p_0y^{p_0-1}-tp_1(1-y)^{p_1-1}\stackrel{!}{=}0.}
  We will discuss some special cases.
  \paragraph{For $p_0=1$, $p_1=2$}
  Check for local extrema:
  \eq{1-2t(1-y)=0\gdw y = 1-\frac{1}{2t}.}
  Checking the sign of the second derivative we see that this is indeed a
  minimum and lies in our range for $t\ge\frac{1}{2}$ and is lower than the
  boundary value. For $t\le\frac{1}{2}$ the minimum is on the boundary $y=0$.
  Thus we have
  \eq{F(t, 1, 2) = \mcases{t & t < \frac{1}{2}\\ 1 - \frac{1}{4t} & t \ge \frac{1}{2}}}
  and
  \eq{\Theta(\eta, 1, 2) &= \int_0^\frac{1}{2} t^{-\eta -1} t \d t
                           + \int_\frac{1}{2}^\infty t^{-\eta -1} \left(1-\frac{1}{4t}\right) \d t\\
                         &= \left[ \frac{1}{-\eta+1}t^{-\eta+1} \right]_0^\frac{1}{2}
                           + \left[ \frac{1}{-\eta}t^{-\eta} \right]_\frac{1}{2}^\infty
                           - \frac{1}{4}\left[ \frac{1}{-\eta-1}t^{-\eta-1} \right]_\frac{1}{2}^\infty\\
                         &= \frac{1}{1-\eta}2^{\eta-1} + \frac{1}{\eta}2^\eta +\frac{1}{4}\frac{1}{-(\eta+1)}2^{\eta+1}\\
                         &= 2^\eta\left(\frac{1}{2(1-\eta)} + \frac{1}{\eta} +\frac{1}{2}\frac{1}{-(\eta+1)}\right)
                         = \frac{2^\eta}{(1-\eta)\eta(\eta+1)}.
  }
  \paragraph{For $p_0=\frac{1}{2}$, $p_1=\frac{3}{2}$}
  Check for local extrema:
  \eq{\frac{1}{2}y^{-\frac{1}{2}}-\frac{3}{2}t(1-y)^\frac{1}{2}=0\gdw \frac{1}{4y} = \frac{9}{4}t^2(1-y)\gdw
  \frac{1}{9t^2} = (1-y)y\gdw y = \frac{1\pm\sqrt{1-\frac{4}{9t^2}}}{2}.}
  The second (``+''-case) of which is a local minimum for $t>\frac{2}{3}$, but
  only becomes the global infimum at some $t=t^*$. Before this
  point the boundary value at $y=0$ yields the lower value.\\

  $t^*$ is the solution of
  \eq{t\stackrel{!}{=} \left(\frac{1+\sqrt{1-\frac{4}{9t^2}}}{2}\right)^\frac{1}{2}
      + t\left(\frac{1-\sqrt{1-\frac{4}{9t^2}}}{2}\right)^\frac{3}{2}.
  }
  With the substitution $w = \sqrt{1-\frac{4}{9t^2}}$, $t=\frac{2}{3}\sqrt{\frac{1}{1-w^2}}$ we gain
  \eq{
    1 &= \frac{3}{2}\sqrt{1-w^2}\sqrt{\frac{1+w}{2}}+\sqrt{\frac{1-w}{2}}^3\\
      &= \frac{3}{2}\sqrt{1-w}\frac{1+w}{\sqrt{2}}+\sqrt{1-w}\frac{1-w}{2\sqrt{2}}\\
      &= \frac{\sqrt{1-w}(2+w)}{\sqrt{2}}.
  }
  Squaring and factoring we yield
  \eq{0=w^3+3w^2-2=(w+1)(w+1+\sqrt{3})(w-\sqrt{3}+1),}
  of which $w^*=\sqrt{3}-1$ is the only positive solution
  which gives us $t^*=\frac{2}{3}\sqrt{\frac{1}{1-(\sqrt{3}-1)^2}}=\frac{2}{3}\sqrt{1+\frac{2}{\sqrt{3}}}$.
  Thus we have
  \eq{F\left(t, \frac{1}{2}, \frac{3}{2}\right) = \mcases{t & t \le t^*\\ \left(\frac{1+\sqrt{1-\frac{4}{9t^2}}}{2}\right)^\frac{1}{2}
      + t\left(\frac{1-\sqrt{1-\frac{4}{9t^2}}}{2}\right)^\frac{3}{2} & t \ge t^*}}
  and
  \eq{\Theta\left(\eta, \frac{1}{2}, \frac{3}{2}\right) &= \int_0^{t^*} t^{-\eta -1} t \d t
                           + \int_{t^*}^\infty t^{-\eta -1} \left(\frac{1+\sqrt{1-\frac{4}{9t^2}}}{2}\right)^\frac{1}{2}\d t
                           + \int_{t^*}^\infty t^{-\eta -1} t\left(\frac{1-\sqrt{1-\frac{4}{9t^2}}}{2}\right)^\frac{3}{2}\d t\\
                         &= \frac{{t^{*}}^{1-\eta}}{1-\eta}
                           + \frac{1}{\sqrt{2}}\int_{t^*}^\infty t^{-\eta -1} \left(1+\sqrt{1-\frac{4}{9t^2}}\right)^\frac{1}{2}\d t
                           + \frac{1}{2\sqrt{2}}\int_{t^*}^\infty t^{-\eta} \left(1-\sqrt{1-\frac{4}{9t^2}}\right)^\frac{3}{2}\d t\\
                         &= \frac{{t^{*}}^{1-\eta}}{1-\eta}
                           + \frac{9}{4\sqrt{2}}\int_{w^*}^1 w \left(\frac{2}{3\sqrt{1-w}\sqrt{1+w}}\right)^{-\eta +2} (1+w)^\frac{1}{2}\d w\\&\quad
                           + \frac{9}{8\sqrt{2}}\int_{w^*}^1 w \left(\frac{2}{3\sqrt{1-w}\sqrt{1+w}}\right)^{-\eta +3} (1-w)^\frac{3}{2}\d w\\
                         &= \frac{{t^{*}}^{1-\eta}}{1-\eta}
                           + \frac{1}{\sqrt{2}}\left(\frac{3}{2}\right)^\eta\int_{w^*}^1 w (1-w)^{\frac{\eta}{2}-1}(1+w)^{\frac{\eta}{2}-\frac{1}{2}}\d w\\&\quad
                           + \frac{1}{3\sqrt{2}}\left(\frac{3}{2}\right)^\eta\int_{w^*}^1 w (1-w)^{\frac{\eta}{2}}(1+w)^{\frac{\eta}{2}-\frac{3}{2}}\d w.
  }

  \paragraph{For $p_0=\frac{1}{2}$, $p_1=1$}
  Check for local extrema:
  \eq{\frac{1}{2}y^{-\frac{1}{2}}-t=0\gdw y=\frac{1}{4t^2}.}
  Looking at the sign of the second derivate, we see that this is a maximum, so our infimum is one of the
  boundary values, $y=0$ for $t\le 1$ and $y=1$ for $t\ge 1$.
  Thus we have
  \eq{F\left(t, \frac{1}{2}, 1\right) = \mcases{t & t \le 1\\ 1 & t \ge 1} = \min\m{1,t}}
  and
  \eq{\Theta\left(\eta, \frac{1}{2}, 1\right) &= \int_0^1 t^{-\eta -1} t \d t
                           + \int_1^\infty t^{-\eta -1}\d t\\
                         &= \left[ \frac{1}{-\eta+1}t^{-\eta+1} \right]_0^1
                           + \left[ \frac{1}{-\eta}t^{-\eta} \right]_1^\infty \\
                         &= \frac{1}{1-\eta} + \frac{1}{\eta} = \frac{1}{\eta(1-\eta)}.
  }
  \paragraph{For $p_0=1$, $p_1=\frac{3}{2}$}
  Check for local extrema:
  \eq{1-\frac{3}{2}t(1-y)^\frac{1}{2}=0\gdw y = 1 - \frac{4}{9t^2}.}
  It exists for $t>\frac{2}{3}$ and looking at the sign of the second derivate,
  we see that this is a minimum. It is for its range of existence also the
  global minimum. For $t\le\frac{2}{3}$ we have a boundary mimimum at $y=0$.
  Thus we have
  \eq{F\left(t, 1, \frac{3}{2}\right) = \mcases{t & t \le \frac{2}{3}\\
    1-\frac{4}{9t^2}+t\left(\frac{4}{9t^2}\right)^\frac{3}{2} & t \ge \frac{2}{3}}}
  and
  \eq{\Theta(\eta, 1, \frac{3}{2}) &= \int_0^\frac{2}{3} t^{-\eta -1} t \d t
                           + \int_\frac{2}{3}^\infty t^{-\eta -1} \left(1-\frac{4}{9t^2}+t\left(\frac{4}{9t^2}\right)^\frac{3}{2}\right)\d t\\
                         &= \left[ \frac{1}{-\eta+1}t^{-\eta+1} \right]_0^\frac{2}{3}
                           + \int_\frac{2}{3}^\infty t^{-\eta -1} \d t
                           - \frac{4}{9}\int_\frac{2}{3}^\infty t^{-\eta -3} \d t
                           + \frac{8}{27}\int_\frac{2}{3}^\infty t^{-\eta -3} \d t \\
                         &= \left[ \frac{1}{-\eta+1}t^{-\eta+1} \right]_0^\frac{2}{3}
                           + \left[ \frac{1}{-\eta}t^{-\eta} \right]_\frac{2}{3}^\infty
                           - \frac{4}{27}\left[ \frac{1}{-\eta-2}t^{-\eta-2} \right]_\frac{2}{3}^\infty\\
                         &= \frac{2}{3}\frac{1}{1-\eta}\left(\frac{2}{3}\right)^{-\eta}
                           + \frac{1}{\eta}\left(\frac{2}{3}\right)^{-\eta}
                           - \frac{1}{3}\frac{1}{2+\eta}\left(\frac{2}{3}\right)^{-\eta} \\
                         &= \frac{2}{\eta(1-\eta)(2+\eta)}\left(\frac{2}{3}\right)^{-\eta}\\
                         &= \frac{3^\eta 2^{1-\eta}}{\eta(1-\eta)(2+\eta)}.
  }
  \paragraph{For $p_0=\frac{3}{2}$, $p_1=2$}
  Check for local extrema:
  \eq{\frac{3}{2}y^{\frac{1}{2}}-2t(1-y)=0\gdw \frac{3}{2}y^{\frac{1}{2}}+2ty-2t=0.}
  With the substitution $w=y^\frac{1}{2}$ we have
  \eq{\frac{3}{2}w+2tw^2-2t=0\gdw w = \frac{-\frac{3}{2}\pm\sqrt{\frac{9}{4}+16t^2}}{4t}.}
  But since $w>0$ only the ``+''-Solution is possible here.
  So we have
  \eq{y = w^2 = \frac{9}{32t^2} + 1 -\frac{3}{4t}\sqrt{\frac{9}{64t^2}+1} = \frac{9}{32t^2}\left(1 - \sqrt{1+\frac{64t^2}{9}}\right) + 1}
  which is a mimimum that exists for all $t>0$ and is the global minimum for the function.
  Thus we have
  \eq{F\left(t, \frac{3}{2}, 2\right)
  = \left(\frac{9}{32t^2}\left(1 - \sqrt{1+\frac{64t^2}{9}}\right) + 1\right)^{\frac{3}{2}}
  + t\cdot\left(\frac{9}{32t^2}\left(1 - \sqrt{1+\frac{64t^2}{9}}\right)\right)^2.}
\end{proof}
\end{cor}
