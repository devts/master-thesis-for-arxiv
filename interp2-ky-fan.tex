\subsection{Interpolation with Ky-Fan}\label{intp2kf}
\begin{thm}[Ky-Fan inequality, \cite{kyfanineq}]\label{ky-fan}
  For $H_0$ and $H_1$ as defined in Thm \ref{zerlH} it holds that
  \eq{\abs{E_{n+m-1}(H)}\le\abs{E_n(H_0)}+\abs{E_m(H_1)}.}
  \begin{proof}
    \cite{kyfanineq} proves this for the case of completely continuous operators.
    This can be extended to our case by restricting all three operators to the
    span of the eigenvectors of the first $n+m-1$ eigenvalues of all three
    operators.
    These restricted operators are then of finite rank and
    therefore compact and completely continuous but its lowest $n+m-1$
    eigenvalues are still the same by construction as restriction containing the
    respective eigenspaces.
  \end{proof}
\end{thm}
\begin{thm}\label{thmkfint}
  We have
  \eq{\sum_{k}\abs{E_k(H)}^\gamma\le L_{\gamma,d}\int_{\R^d}\abs{V}^{\gamma+\kappa}\d x}
  with $\kappa=\frac{d}{2}$ and
  \eq{L_{\gamma,d} \le \frac{\Theta(\eta, p_0+\kappa, p_1+\kappa)}{\Theta(\eta, p_0, p_1)}
  (1+N)^{1-\eta}\theta^{-\kappa+\kappa\eta}L^{1-\eta}_{p_0,d}
  (1+N^{-1})^\eta(1-\theta)^{-\kappa\eta}L^\eta_{p_1,d}}
  where $\gamma = (1-\eta)p_0+\eta p_1$, i.e. $\eta = \frac{\gamma - p_0}{p_1-p_0}$.
  \begin{proof}
    We follow the proof of Weidl in \cite{weidlinterpolation}, which gives the
    proof for $d=1$ but can be easily extended to general $d$ as we will show
    here:
    For $\N\ni N\ge 1$ we define
    \eq{a_k:=\abs{E_s(H_0)} &\qquad s=1+\floor{\frac{k}{N+1}}\\
        b_k:=\abs{E_l(H_1)} &\qquad l=N\floor{\frac{k}{N+1}} + (k \mod (N + 1)).}
    Then we can use Ky-Fan to derive
    \eq{\abs{E_k(H)} \stackrel{\text{Thm \ref{ky-fan}}}{\le} a_k + b_k} 
    %Ex. N = 2
    %1 2 3 4 5 6 k
    %1 1 2 2 2 3 a_k
    %1 2 2 3 4 4 b_k
    and use the definition of the sequences above to derive the following two estimates:
    \eq{\norm{(a_k)}_{l_0}^{p_0} \le (1+N)\sum_k \abs{E_k(H_0)}^{p_0} &\qquad \text{since every index is hit $N+1$ times due to the floor}\\
        \norm{(b_k)}_{l_1}^{p_1} \le (1+N^{-1})\sum_k \abs{E_k(H_1)}^{p_1} &\qquad
        \parbox[t]{25em}{since every index $l$ is hit once except for indices that are multiples of N
              who are hit twice ($l=zN$ is hit by $k=zN, z(N+1)$), whose weight is evenly redistributed as $N^{-1}$
              on the other indices which is possilbe since the eigenvalues are ordered decreasing in magnitude.}}
    Using these inequalities we gain
    \eq{K((E_k(H)), t, p_0, p_1) &\le K((a_k + b_k), t, p_0, p_1) \\
            \intertext{since $K$ is defines as an infimum,
               choosing the particular split $u_k^{(0)}=a_k$, $u_k^{(1)}=b_k$ in Def. \ref{defK} we gain}
        &\le \norm{a_k}_{l_{p_0}}^{p_0} + t\norm{b_k}_{l_{p_1}}^{p_1} \\
        &\le (1+N)\sum_k \abs{E_k(H_0)}^{p_0} + t(1+N^{-1})\sum_k \abs{E_k(H_1)}^{p_1} \\
        \overset{\text{Thm \ref{abschEHi}}}&{\le} (1+N)\theta^{-\kappa}L_{p_0,d}\norm{V_0}_{L_{p_0+\kappa}}^{p_0+\kappa}
             + t(1+N^{-1})(1-\theta)^{-\kappa} L_{p_1,d}\norm{V_1}_{L_{p_1+\kappa}}^{p_1+\kappa} \\
        &= (1+N)\theta^{-\kappa}L_{p_0,d}\left(\norm{V_0}_{L_{p_0+\kappa}}^{p_0+\kappa}
             + t\frac{(1+N^{-1})(1-\theta)^{-\kappa}L_{p_1,d}}{(1+N)\theta^{-\kappa}L_{p_0,d}}\norm{V_1}_{L_{p_1+\kappa}}^{p_1+\kappa}\right).
    }
    If the definitions of $s$ and $l$ are interchanged, $N$ will simply switch places with $N^{-1}$.
    Thus we can choose $N$ as $k$ or $\frac{1}{k}$ for any non-zero $k\in\N$.
    Taking the infimum over all possible decompositions $V_0$ and $V_1$ we thus obtain
    \eq{K((E_k(H)), t, p_0, p_1) \le (1+N)\theta^{-\kappa}L_{p_0,d}
        K\left(V, t\frac{(1+N^{-1})(1-\theta)^{-\kappa}L_{p_1,d}}{(1+N)\theta^{-\kappa}L_{p_0,d}}, p_0+\kappa, p_1+\kappa\right).}
    Applying $\Phi_{\eta, 1}$ (which is possible thanks to Thm \ref{thmfineq})
    to the last inequality we yield with Thm \ref{eta1eq} and Thm \ref{Phitfactor}
    \eq{\Theta(\eta, p_0, p_1) \norm{(E_k(H))}_{l_\gamma}^\gamma\le (1+N)\theta^{-\kappa}L_{p_0,d}
        \left(\frac{(1+N^{-1})(1-\theta)^{-\kappa}L_{p_1,d}}{(1+N)\theta^{-\kappa}L_{p_0,d}}\right)^\eta
        \Theta(\eta, p_0+\kappa, p_1+\kappa)\norm{V}_{L_{\gamma+\kappa}}^{\gamma+\kappa}.
    }
  \end{proof}
\end{thm}
\begin{defn}\label{defM}
  We define
  \eq{M(\eta) := \inf_{\substack{N=k,\frac{1}{k}\\1\le k\in\N}}(1+N)^{1-\eta}(1+N^{-1})^\eta}
  in order to summarize the influence of $N$-related terms into a dedicated function.
\end{defn}
\begin{thm}\label{opttheta}
  To optimize the choice of $\theta$ in Thm. \ref{thmkfint} we calculate
  \eq{
    \inf_{\theta \in (0,1)} \theta^{-\kappa+\kappa\eta}(1-\theta)^{-\kappa\eta}
    = (1- \eta)^{-\kappa(1-\eta)}\eta^{-\kappa\eta}.
  }
  \begin{proof}
    For convinience we define
    \eq{f := \theta^{-\kappa(1-\eta)}(1-\theta)^{-\kappa\eta}.}
    To obtain the infimum we calculate
    \eq{\frac{\d f}{\d\theta} = f\cdot(\kappa(\eta-1)\theta^{-1} +\kappa\eta(1-\theta)^{-1}) \overset{!}{=} 0}
    and solving the equation yields
    \eq{\theta = 1 - \eta.}
  \end{proof}
\end{thm}
\begin{cor}\label{corkfint}
  By Thms \ref{defM}, \ref{opttheta} we obtain
  \eq{L_{\gamma,d} \le \frac{\Theta(\eta, p_0+\kappa, p_1+\kappa)}{\Theta(\eta, p_0, p_1)}
  M(\eta)(1-\eta)^{-\kappa(1-\eta)}\eta^{-\kappa\eta}L^{1-\eta}_{p_0,d}L^\eta_{p_1,d}.}
\end{cor}

\subsubsection{Unified notation for results and plots}
With $C_{\gamma,d}^{(a,b), \text{kf}}$, $\gamma\in (a,b)$ we denote the upper bound that has been
achieved by Ky-Fan (kf) interpolation with $p_0=a$, $p_1=b$, $L_{{p_i,d}} =
C_{p_i,d}^\text{lit}L_{p_i,d}^\text{cl}$ ($i=0,1$).
\eq{C_{\gamma,d}^{\m{(a,b),(b,c)}} := C_{\gamma,d}^{(a,b)}\chi((a,b)) + C_{\gamma,d}^{(b,c)}\chi((b,c))}

\subsubsection{Numerical Results for interpolation between $\frac{1}{2}$ and $\frac{3}{2}$ in $d=1$}
Here we apply the interpolation once for $p_0=\frac{1}{2}$ and $p_1=\frac{3}{2}$.
\plotinc{s1d_kf_total_0.tex}
{Numeric evaluation of the achieved constants for $d=1$, $p_0=\frac{1}{2}$, $p_1=\frac{3}{2}$,
$L_{\frac{1}{2},1}/L_{\frac{1}{2},1}^\text{cl}=2$, $L_{\frac{3}{2},1}/L_{\frac{3}{2},1}^\text{cl}=1$
in comparison with the known results from Def \ref{defClit}.}
{plot:interp-ky-fan-res-total}
\plotinc{s1d_kf_fact_0.tex}
{Numeric evaluation of the constituting factors for $d=1$, $p_0=\frac{1}{2}$, $p_1=\frac{3}{2}$,
$L_{\frac{1}{2},1}/L_{\frac{1}{2},1}^\text{cl}=2$, $L_{\frac{3}{2},1}/L_{\frac{3}{2},1}^\text{cl}=1$.}
{plot:interp-ky-fan-res-fact}

\subsubsection{Numerical Results for separate interpolation between $\frac{1}{2}$, $1$ and $1$, $\frac{3}{2}$ in $d=1$}
Here we apply the interpolation twice separately for $p_0=\frac{1}{2}$, $p_1=1$ and $p_0=1$, $p_1=\frac{3}{2}$.
\plotinc{s1d_kf_total_12.tex}
{Numeric evaluation of the achieved constants for $d=1$,
$p_0=\frac{1}{2}$, \{$p_0=\frac{1}{2}$, $p_1=1$\} and \{$p_0=1$, $p_1=\frac{3}{2}$\},
$L_{\frac{1}{2},1}/L_{\frac{1}{2},1}^\text{cl}=2$, $L_{1,1}/L_{\frac{3}{2},1}^\text{cl}\le 1.456$,
$L_{\frac{3}{2},1}/L_{\frac{3}{2},1}^\text{cl}=1$.}
{plot:interp-ky-fan-res-total-12}
\plotinc{s1d_kf_fact_12.tex}
{Numeric evaluation of the constituting factors for $d=1$,
$p_0=\frac{1}{2}$, \{$p_0=\frac{1}{2}$, $p_1=1$\} and \{$p_0=1$, $p_1=\frac{3}{2}$\},
$L_{\frac{1}{2},1}/L_{\frac{1}{2},1}^\text{cl}=2$, $L_{1,1}/L_{\frac{3}{2},1}^\text{cl}\le 1.456$,
$L_{\frac{3}{2},1}/L_{\frac{3}{2},1}^\text{cl}=1$.}
{plot:interp-ky-fan-res-fact-12}

\subsubsection{Numerical Results in $d=1$}
When comparing the different Ky-Fan interpolations, we see that the
interpolation on the whole interval $[0.5,1]$ reaches the best results in
$(1.440,1.5)$, while the best results in $(0.975,1)$ are achieved by the
interpolation on the interval $[0.5,1]$.
Outside of these areas the estimate given by the monotonicity remains the best result.
\plotinc{s1d_kf_total_comp.tex}
{Numeric evaluation of the Results for Ky-Fan-interpolation for $d=1$,
\{$p_0=\frac{1}{2}$, $p_1=\frac{3}{2}$\}, \{$p_0=\frac{1}{2}$, $p_1=1$\} and \{$p_0=1$, $p_1=\frac{3}{2}$\}, 
$L_{\frac{1}{2},1}/L_{\frac{1}{2},1}^\text{cl}=2$, $L_{1,1}/L_{1,1}^\text{cl}\le 1.456$,
$L_{\frac{3}{2},1}/L_{\frac{3}{2},1}^\text{cl}=1$.}
{plot:interp-kf-total-comp}
