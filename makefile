all: bin/ma.pdf bin/arxiv.tar.gz

bin/ma.pdf: *.tex *.pdf
	mkdir -p bin
	pdflatex --output-directory=bin ./ma.tex
	pdflatex --output-directory=bin ./ma.tex
	pdflatex --output-directory=bin ./ma.tex

bin/arxiv.tar.gz: *.tex *.pdf
	mkdir -p bin
	tar cvaf bin/arxiv.tar.gz 00README.XXX *.tex *.pdf
