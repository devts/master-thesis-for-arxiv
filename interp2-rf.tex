\subsection{Interpolation with Rotfeld-Inequality}\label{intp2rf}
We now adapt the previous proof to work with a Rotfeld-Inequality instead of
Ky-Fan.
\begin{thm}\label{thmrfint}
  We have
  \eq{\sum_{k}\abs{E_k(H)}^\gamma\le L_{\gamma,d}\int_{\R^d}\abs{V}^{\gamma+\kappa}\d x}
  with $\kappa=\frac{d}{2}$ and
  \eq{
    L_{\gamma,d} \le \frac{\Theta(\eta, p_0+\kappa, p_1+\kappa)}{\Theta(\eta, p_0, p_1)}
    a(\gamma)^{(1-\eta)}b(\gamma)^\eta
    \theta^{-\kappa(1-\eta)}(1-\theta)^{-\kappa\eta}L^{1-\eta}_{p_0,d}L^\eta_{p_1,d}
  }
  where $\gamma = (1-\eta)p_0+\eta p_1$, i.e. $\eta = \frac{\gamma - p_0}{p_1-p_0}$
  and $a(\gamma),b(\gamma)$ are a choice of constants $a(\gamma,\vartheta)$, $b(\gamma,\vartheta)$
  for an arbitrary but fixed $\vartheta$ from Thm \ref{rfineq}.
  \begin{proof}
    While the result save for the coefficients looks strikingly similar to the
    Ky-Fan interpolation, the proof works very different.
    This is because while the Ky-Fan inequality provides us with an an estimate
    of the individual $E_k(K)$, the Rotfeld Inequality provides us only with an
    inequality for the Riesz mean as a whole. Thus a conceptually different
    proof is needed.\\

    As $K$ is defined as an infimum we gain
    \eq{ K(E_k(H_0), t, p_0, p_1) \le \norm{E_k(H_0)}_{l_{p_0}}^{p_0}}
    by choosing $u_n^{(1)}=0$ and
    \eq{ K(E_k(H_1), t, p_0, p_1) \le t\norm{E_k(H_1)}_{l_{p_1}}^{p_1}}
    by choosing $u_n^{(0)}=0$.
    This allows us to derive the following inequality for an arbitrary but
    fixed $\alpha\in\R$:
    \eq{
      K(E_k(H_0), t, p_0, p_1) + \alpha K(E_k(H_1), t, p_0, p_1)
      \overset{\text{Def as inf}}&{\le} \norm{E_k(H_0)}_{l_{p_0}}^{p_0} + \alpha t\norm{E_k(H_1)}_{l_{p_1}}^{p_1} \\
      \overset{\text{Thm \ref{abschEHi}}}&{\le}
        \theta^{-\kappa}L_{p_0,d}\norm{V_0}_{l_{p_0+\kappa}}^{p_0+\kappa}
        + \alpha t(1-\theta)^{-\kappa} L_{p_1,d}\norm{V_1}_{L_{p_1+\kappa}}^{p_1+\kappa} \\
      &= \theta^{-\kappa}L_{p_0,d}\left(\norm{V_0}_{L_{p_0+\kappa}}^{p_0+\kappa}
        + \alpha t \theta^\kappa(1-\theta)^{-\kappa}L_{p_0,d}^{-1}L_{p_1,d}\norm{V_1}_{L_{p_1+\kappa}}^{p_1+\kappa}\right)
    }
    Taking $\inf_{V=V_0+V_1}$ over this equation we yield
    \eq{\label{prfK}
      \inf_{V=V_0+V_1} K(E_k(H_0), &t, p_0, p_1) + \alpha K(E_k(H_1), t, p_0, p_1) \\
      &\le \theta^{-\kappa}L_{p_0,d}K(V, \alpha\theta^\kappa(1-\theta)^{-\kappa}L_{p_0,d}^{-1}L_{p_1,d} t, p_0 + \kappa, p_1 + \kappa) \\
      \overset{\text{Thm \ref{Phitfactor}}}&{=}
        \theta^{-\kappa(1 - \eta)}(1-\theta)^{-\kappa\eta}\alpha^\eta L_{p_0, d}^{1-\eta}L_{p_1, d}^\eta K(V, t, p_0 + \kappa, p_1 + \kappa).
    }
    Since $\Phi_{\eta,1}$ is monotonic (Thm. \ref{thmfineq}) it follows that
    \eq{\label{prfPhi}
      &\Phi_{\eta,1}\left[\inf_{V=V_0+V_1} K(E_k(H_0), t, p_0, p_1) + \alpha K(E_k(H_1), t, p_0, p_1)\right] \\
      &\mkern150mu \le \Phi_{\eta,1}\left[\theta^{-\kappa(1 - \eta)}(1-\theta)^{-\kappa\eta}\alpha^\eta
        L_{p_0, d}^{1-\eta}L_{p_1, d}^\eta K(V, t, p_0 + \kappa, p_1 + \kappa)\right].
    }
    With the help of this somewhat odd looking inequality we can now derive our main result:
    \eq{
      \norm{E_k(H)}_{l_\gamma}^\gamma &= \norm{E_k(H_0+H_1)}_{l_\gamma}^\gamma\\
      \thmosal{rfineq}{\le} 
        a(\gamma) \norm{E_k(H_0)}_{l_\gamma}^\gamma + b(\gamma) \norm{E_k(H_1)}_{l_\gamma}^\gamma \\
      \thmosal{eta1eq}{=} 
        \Theta(\eta, p_0, p_1)^{-1}\left(a(\gamma)\Phi_{\eta,1}[K(E_k(H_0), t, p_0, p_1)] + b(\gamma)\Phi_{\eta,1}[K(E_k(H_1), t, p_0, p_1)]\right) \\
      \thmosal{phi1linear}{=} \Theta(\eta, p_0, p_1)^{-1}a(\gamma)\Phi_{\eta,1}
          [K(E_k(H_0), t, p_0, p_1) + \frac{b(\gamma)}{a(\gamma)} K(E_k(H_1), t, p_0, p_1)]
    }
    Taking $\inf_{V=V_0+V_1}$ over this equation we yield
    \eq{
      \norm{E_k(H)}_{l_\gamma}^\gamma &\le \inf_{V=V_0+V_1} \Theta(\eta, p_0, p_1)^{-1}a(\gamma)\Phi_{\eta,1}
        \left[K(E_k(H_0), t, p_0, p_1) + \frac{b(\gamma)}{a(\gamma)} K(E_k(H_1), t, p_0, p_1)\right] \\
      &=  \Theta(\eta, p_0, p_1)^{-1}a(\gamma)\Phi_{\eta,1}
        \left[\inf_{V=V_0+V_1} K(E_k(H_0), t, p_0, p_1) + \frac{b(\gamma)}{a(\gamma)} K(E_k(H_1), t, p_0, p_1)\right] \\
      \overset{\text{\eqref{prfPhi} with }\alpha = \frac{b(\gamma)}{a(\gamma)}}&{\le}
        \Theta(\eta, p_0, p_1)^{-1}a(\gamma)\Phi_{\eta,1}
        \left[\theta^{-\kappa(1 - \eta)}(1-\theta)^{-\kappa\eta}\left(\frac{b(\gamma)}{a(\gamma)}\right)^\eta
        L_{p_0, d}^{1-\eta}L_{p_1, d}^\eta K(V, t, p_0 + \kappa, p_1 + \kappa)\right] \\
      \overset{\text{Thm \ref{phi1linear}}}&{=}
        \Theta(\eta, p_0, p_1)^{-1}a(\gamma)\theta^{-\kappa(1 - \eta)}(1-\theta)^{-\kappa\eta}
        \left(\frac{b(\gamma)}{a(\gamma)}\right)^\eta L_{p_0, d}^{1-\eta}L_{p_1, d}^\eta
        \Phi_{\eta,1}[K(V, t, p_0 + \kappa, p_1 + \kappa)]\\
      \thmosal{eta1eq}{=} 
        \frac{\Theta(\eta,  p_0 + \kappa, p_1 + \kappa)}{\Theta(\eta, p_0, p_1)}a(\gamma)^{1-\eta}b(\gamma)^\eta
        \theta^{-\kappa(1 - \eta)}(1-\theta)^{-\kappa\eta}
        L_{p_0, d}^{1-\eta}L_{p_1, d}^\eta \norm{V}_{L_{\gamma+\kappa}}^{\gamma+\kappa}
    }
  \end{proof}
\end{thm}
\begin{defn}\label{defY}
  We define
  \eq{Y(\eta, \gamma) := \inf_{a(\gamma), b(\gamma)\text{ from Thm \ref{rfineq}}}a(\gamma)^{1-\eta}b(\gamma)^\eta
                  = \inf_{\vartheta\in(0,1)}a(\gamma,\vartheta)^{1-\eta}b(\gamma,\vartheta)^\eta}
  in order to summarize the influence of Rotfeld-related $a$ and $b$ terms into a dedicated function.
\end{defn}
\begin{thm}
  To optimize the choice of $\vartheta$ in Thm. \ref{thmrfint} we calculate
  \eq{
    Y(\eta, \gamma) = \begin{cases}
      1 & \gamma \le 1 \\
      (1-\eta)^{(-\gamma+1)(1-\eta)}\eta^{(-\gamma+1)\eta} & \gamma > 1.
    \end{cases}
  }
  \begin{proof}
    With a quick calculation it can be seen that
    \eq{\inf_{\vartheta\in (0,1)} \vartheta^{(-\gamma+1)(1-\eta)}(1-\vartheta)^{(-\gamma+1)\eta} 
         = (1-\eta)^{(-\gamma+1)(1-\eta)}\eta^{(-\gamma+1)\eta}.}
  \end{proof}
\end{thm}
\begin{cor}\label{corrfint}
  By Thms \ref{defY}, \ref{opttheta} we obtain
  \eq{L_{\gamma,d} \le \frac{\Theta(\eta, p_0+\kappa, p_1+\kappa)}{\Theta(\eta, p_0, p_1)}
  Y(\eta, \gamma)(1-\eta)^{-\kappa(1-\eta)}\eta^{-\kappa\eta}L^{1-\eta}_{p_0,d}L^\eta_{p_1,d}.}
\end{cor}
\begin{rem}
  For $\gamma\le\frac{3}{2}$ we have
  \eq{Y(\eta,\gamma)\le\sqrt{M(\eta)}}
  and for $\gamma\le 2$ we have
  \eq{Y(\eta,\gamma)\le M(\eta)}
  so our Rotfeld based interpolation results are always better than the Ky-Fan
  based ones for $\gamma\le 2$.
  \begin{proof}
    For $\gamma \le 1$ we have $Y(\eta,\gamma) = 1$, while $M(\eta)>1$ so we
    only need to take care of the $\gamma > 1$ case in the following.
    Since the exact definition of $M$ with its discrete infimum is somewhat
    bothersome to use we calculate
    \eq{M(\eta) \ge \inf_{0<x\in\R}(1+x)^{1-\eta}(1+x^{-1})^\eta = (1-\eta)^{-(1-\eta)}\eta^{-\eta}.}
    For $\gamma\le\frac{3}{2}$ we have $1-\gamma\ge -\frac{1}{2}$ and thus
    \eq{Y(\eta, \gamma) = (1-\eta)^{(-\gamma+1)(1-\eta)}\eta^{(-\gamma+1)\eta}
      \le (1-\eta)^{-\frac{1}{2}(1-\eta)}\eta^{-\frac{1}{2}\eta}
      \le \sqrt{M(\eta)}.
    }
    For $\gamma\le 2$ we have $1-\gamma\ge -1$ and thus
    \eq{Y(\eta, \gamma) = (1-\eta)^{(-\gamma+1)(1-\eta)}\eta^{(-\gamma+1)\eta}
      \le (1-\eta)^{-(1-\eta)}\eta^{-\eta}
      = M(\eta).
    }
  \end{proof}
\end{rem}

\subsubsection{Unified notation for results and plots}
With $C_{\gamma,d}^{(a,b), \text{rf}}$, $\gamma\in (a,b)$ we denote the upper bound that has been
achieved by Rotfeld (rf) interpolation with $p_0=a$, $p_1=b$, $L_{{p_i,d}} =
C_{p_i,d}^\text{lit}L_{p_i,d}^\text{cl}$ ($i=0,1$).
\eq{C_{\gamma,d}^{\m{(a,b),(b,c)}} := C_{\gamma,d}^{(a,b)}\chi((a,b)) + C_{\gamma,d}^{(b,c)}\chi((b,c))}
An additonal superscript ``op'' denotes an operator valued result
(e.g. $C_{\gamma,d}^{(a,b), \text{rf, op}}$ etc.).

\input{interp2-rf-num.tex}
