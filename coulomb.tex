\section{Shifted Coulomb potential}
Since the Coulomb potential is among the most important potentials in
mathematical physics we will take a look at what happens to the ratio studied
by the Lieb-Thirring inequality when such an operator is shifted such that it
retains a finite but nonzero number of eigenvalues.
\label{sec:lti_scoulomb}
\begin{thm}[Coulomb potential in dimension $d$, \cite{weidlbook}]
  \label{coulombew}
  The operator
  \eq{H_0 := -\laplace-\abs{x}^{-1}}
  on $L_2(\R^d)$ has the eigenvalues
  \eq{e_k = -\frac{1}{(2k+d-1)^2}}
  with multiplicity
  \eq{\frac{(k+d-1)!(2k+d-1)}{(d-1)!k!}}
  with $k\ge 0$.
\end{thm}
\begin{defn}
  Now we consider the shifted Coulomb potential in dimension $d$,
  \eq{H_\lambda = -\laplace-\abs{x}^{-1} + \lambda}
  with $0<\lambda<-e_0=(d-1)^{-2}$.
  We define the sum of the eigenvalues of the $d$-dimensional $H_\lambda$ as
  \eq{S_d(\gamma, \lambda) := \Tr(H_\lambda)_-^\gamma}
  and we define the integral of $V_-^{\gamma+\frac{d}{2}}$ scaled by $L^\text{cl}_{\gamma,d}$ as
  \eq{I_d(\gamma, \lambda) := L^\text{cl}_{\gamma,d} \int_{\R^d}\abs{-\frac{1}{\abs{x}}+\lambda}_-^{\gamma+\frac{d}{2}} \d x}
  so that we can define an analogon of the scaled Lieb-Thirring constants $C_{\gamma,d}$
  restricted to a particular shifted Coulomb potential
  \eq{v_d(\gamma, \lambda) := \frac{S_d(\gamma, \lambda)}{I_d(\gamma, \lambda)}}
  or all shifted Coulomb potentials:
  \eq{v_d(\gamma) := \sup_{\lambda\in (0,(d-1)^{-2})} v_d(\gamma, \lambda).}
\end{defn}
\subsection{Analytic results}
\begin{thm}
  Let $0\le \gamma < \frac{d}{2}$.
  Then
  \eq{I_d(\gamma, \lambda) =
    2^{1-d} \lambda^{\gamma-\frac{d}{2}}
    \frac{\Gamma(\gamma+1)\Gamma(\frac{d}{2}-\gamma)}{\Gamma(d+1)\Gamma(\frac{d}{2})}
  }
  \begin{proof}
    This can be proven by a simple calculation:
    \eq{
      \int_{\R^d}\abs{-\frac{1}{\abs{x}}+\lambda}_-^{\gamma+\frac{d}{2}} \d x
       &= \int_0^{\frac{1}{\lambda}}\abs{-\frac{1}{r}+\lambda}^{\gamma+\frac{d}{2}}
           \frac{2\pi^\frac{d}{2}}{\Gamma(\frac{d}{2})} r^{d-1} \d r \\
       &= \frac{2\pi^\frac{d}{2}}{\Gamma(\frac{d}{2})} \int_0^{\frac{1}{\lambda}}
       \left(\frac{1}{r}-\lambda\right)^{\gamma+\frac{d}{2}} r^{d-1} \d r \\
       &= \frac{2\pi^\frac{d}{2}}{\Gamma(\frac{d}{2})} \int_0^{\frac{1}{\lambda}}
       \lambda^{\gamma+\frac{d}{2}}\left(\frac{1}{\lambda r}-1\right)^{\gamma+\frac{d}{2}} r^{d-1} \d r
          \qquad \text{Subst. $y:=\lambda r$}\\
       &= \frac{2\pi^\frac{d}{2}}{\Gamma(\frac{d}{2})} \int_0^1
          \lambda^{\gamma+\frac{d}{2}}\left(\frac{1}{y}-1\right)^{\gamma+\frac{d}{2}}
          y^{d-1} \lambda^{-(d-1)}\lambda^{-1}\d y \\
       &= \frac{2\pi^\frac{d}{2}}{\Gamma(\frac{d}{2})}\lambda^{\gamma-\frac{d}{2}}
       \int_0^1 \left(\frac{1}{y}-1\right)^{\gamma+\frac{d}{2}} y^{d-1} \d y\\
       &= \frac{2\pi^\frac{d}{2}}{\Gamma(\frac{d}{2})}\lambda^{\gamma-\frac{d}{2}}
       \int_0^1 (1-y)^{\gamma+\frac{d}{2}} y^{-\gamma+\frac{d}{2}-1} \d y\\
       &= \frac{2\pi^\frac{d}{2}}{\Gamma(\frac{d}{2})}\lambda^{\gamma-\frac{d}{2}}
       \frac{\Gamma(\gamma+\frac{d}{2}+1)\Gamma(-\gamma+\frac{d}{2})}{\Gamma(d+1)},
    }
    and by multiplication with the classical constant we immediately obtain the
    desired result
    \eq{
      \int_{\R^d}\abs{-\frac{1}{\abs{x}}+\lambda}_-^{\gamma+\frac{d}{2}} \d x
        \cdot L^\text{cl}_{\gamma,d} =
      2^{1-d} \lambda^{\gamma-\frac{d}{2}}
      \frac{\Gamma(\gamma+1)\Gamma(\frac{d}{2}-\gamma)}{\Gamma(d+1)\Gamma(\frac{d}{2})}.
    }
  \end{proof}
\end{thm}
\begin{thm}
  \eq{S_d(\gamma,\lambda) = \sum_{k=0}^{k^*} \frac{(k+d-2)!(2k+d-1)}{(d-1)!k!}\left(\frac{1}{(2k+d-1)^2}-\lambda\right)^\gamma}
  with the index of the highest strictly negative eigenvalue
  \eq{k^* = fl\left(\frac{1}{2}\left(\frac{1}{\sqrt{\lambda}}-d+1\right)\right)}
  where $fl$ is a modified floor function
  \eq{fl(x) = \mcases{ x - 1 & x\in\Z \\ \lfloor x \rfloor & x\not\in\Z}}
  that rounds down on integers.
  One should note that the difference between $fl$ and floor only makes a
  difference in $S$ if $\gamma=0$ since if we would not round down on integers
  we would count the eigenvalue 0.
  \begin{proof}
    Follows directly from Thm. \ref{coulombew}.
  \end{proof}
\end{thm}
\begin{thm}
  We have
  \eq{
    S_d(0,\lambda) = \frac{1}{d!}\left(\frac{(k^*+(d-1))!}{k^*!}(2k^*+d)\right)
    = \frac{1}{d!}((k^*+1)\cdot\ldots\cdot(k^*+(d-1))(2k^*+d)),
  }
  in particular for $d=2$ we have
  \eq{S_2(0, \lambda) = (k^*+1)^2}
  and for $d=3$ we have
  \eq{S_3(0, \lambda) = \frac{1}{6}(k^*+1)(k^*+2)(2k^*+3).}
  \begin{proof}
    We rewrite the multiplicity to the following form
    \eq{
      \frac{(k+(d-2))!(2k+(d-1))}{(d-1)!k!}
      &= \frac{(k+(d-2))!}{(d-2)!k!} + 2\frac{((k-1)+(d-1))!}{(d-1)!(k-1)!}\\
      &= \binom{k+(d-2)}{k} + 2 \binom{k-1+(d-1)}{k-1},
    }
    which we now use to calculate $S_d(0,\lambda)$:
    \eq{
      S_d(0,\lambda) &= \sum_{k=0}^{k^*} \frac{(k+d-2)!(2k+d-1)}{(d-1)!k!}\\
      &= \sum_{k=0}^{k^*} \binom{k+(d-2)}{k} + 2 \binom{k-1+(d-1)}{k-1}\\
      &= \sum_{k=0}^{k^*} \binom{k+(d-2)}{k} + 2 \sum_{k=0}^{k^*-1}\binom{k+(d-1)}{k}\\
      \interject{we now use the following summation formula for binomial coefficients:}
      {\sum_{k=0}^m \binom{n+k}{k} = \binom{n+m+1}{n+1}}
      &= \binom{(d-2)+k^*+1}{(d-2)+1} + 2 \binom{(d-1)+(k^*-1)+1}{(d-1)+1}\\
      &= \binom{k^*+(d-1)}{d-1} + 2 \binom{k^*+(d-1)}{d}\\
      \interject{we now use the following summation formula for binomial coefficients:}
      {\binom{n}{k} + \binom{n}{k+1} = \binom{n+1}{k+1}}
      &= \binom{k^*+d}{d} + \binom{k^*+(d-1)}{d}\\
      &= \frac{(k^*+d)!}{d!k^*!} + \frac{(k^*+(d-1))!}{d!(k^*-1)!}\\
      &= \frac{1}{d!}\left(\frac{(k^*+d-1)!}{k^*!}(2k^*+d)\right).
    }
  \end{proof}
\end{thm}
\begin{cor}
  Thus we have
  \eq{v_d(0,\lambda) = 2^{d-1}\lambda^{\frac{d}{2}}\left(\frac{(k^*+(d-1))!}{k^*!}(2k^*+d)\right),}
  in particular for $d=2$ we have
  \eq{v_2(0, \lambda) = 4\lambda (k^*+1)^2}
  and for $d=3$ we have
  \eq{v_3(0, \lambda) = 4\lambda^{\frac{3}{2}}(k^*+1)(k^*+2)(2k^*+3).}
  Below we have some plots for small dimensions.
  \plotincxs{out_lscoulomb_v0g_d2.tex}{out_lscoulomb_v0g_d3.tex}
  {Plots of $v_2(0,\lambda)$ (left) and $v_3(0,\lambda)$ (right).}
  {plot:lscoulomb_d2+3_v0g}{0.5}
  \plotincxs{out_lscoulomb_v0g_d4.tex}{out_lscoulomb_v0g_d5.tex}
  {Plots of $v_4(0,\lambda)$ (left) and $v_5(0,\lambda)$ (right).}
  {plot:lscoulomb_d4+5_v0g}{0.5}
  Interestingly, in dimensions $d>5$ the supremum no longer occurs for
  $\lambda\to -e_0=(d-1)^{-2}$ as can be seen in the following plots:
  \plotincxs{out_lscoulomb_v0g_d6.tex}{out_lscoulomb_v0g_d8.tex}
  {Plots of $v_6(0,\lambda)$ (left) and $v_8(0,\lambda)$ (right).}
  {plot:lscoulomb_d6+8_v0g}{0.5}
  Since
  \eq{\frac{(k^*+d-1)!}{k^*!}(2k^*+d) = 2^{1-d}\lambda^{-\frac{d}{2}}
      + \landauO(\lambda^{-\frac{d-1}{2}})\qquad\text{for } \lambda \to 0}
  we have
  \eq{\lim_{\lambda\to 0} v_d(0,\lambda) = 1}
  in any dimension $d$, which implies
  \eq{v_d(0)\ge 1.}
  For $\lambda\to -e_0=(d-1)^{-2}$ we have
  \eq{\lim_{\lambda\to -e_0} v_d(0,\lambda) = \frac{2^{d-1}d!}{(d-1)^2}}
  and thus
  \eq{v_d(0)\ge \frac{2^{d-1}d!}{(d-1)^d}.}
  In particular for dimensions two to five we have
  \eq{v_2(0)\ge 4 \qquad v_3(0)\ge 3 \qquad v_4(0) \ge \frac{64}{27} \qquad v_5(0) \ge \frac{15}{8},}
  which are probably even equalities, as suggested by the numeric results.
  For higher dimensions this bound continues to fall, in particular we have
  \eq{v_6(0)\ge\frac{4608}{3125} \qquad v_7(0)\ge\frac{280}{243} \qquad v_8(0)\ge \frac{737280}{823543}}
  and starting in dimension eight the bound falls under one and can thus not be the supremum anymore.
\end{cor}
\begin{cor}
  In general we have
  \eq{
    S_d(1,\lambda) &= \sum_{k=0}^{k^*} \frac{(k+d-2)!(2k+d-1)}{(d-1)!k!} \left(\frac{1}{(2k+d-1)^2}-\lambda\right)\\
    &= -\lambda S_d(0, \lambda) + \sum_{k=0}^{k^*} \frac{(k+d-2)!}{(2k+d-1)(d-1)!k!}.
  }
  For $d=3$ the latter sum becomes trivial and we get
  \eq{
    S_3(1,\lambda) &= -\lambda S_d(0, \lambda) + \sum_{k=0}^{k^*} \frac{(k+1)!}{4(k+1)k!}\\
    &= -\lambda S_d(0, \lambda) + \frac{1}{4}(k^*+1)
  }
  and since
  \eq{I_3(1,\lambda) = \frac{1}{12}\lambda^{-\frac{1}{2}}}
  we get
  \eq{
    v_3(1,\lambda) &= \left(-2\lambda (k^*+1)(k^*+2)(2k^*+3) + 3(k^*+1)\right)\lambda^{\frac{1}{2}}\\
    &= -\frac{1}{2}v_3(0,\lambda) + 3(k^*+1)\lambda^{\frac{1}{2}}.
  }
  which implies
  \eq{\lim_{\lambda\to 0} v_3(1,\lambda) = 1}
  and
  \eq{v_3(1)\ge 1.}
  Plotting this result suggests that the latter inequality is probably sharp:
  \plotinc{out_lscoulomb_v1g_d3.tex}
  {Plot of $v_3(1,\lambda)$.}
  {plot:lscoulomb_d3_v1g}
\end{cor}
\input{coulomb-num.tex}
