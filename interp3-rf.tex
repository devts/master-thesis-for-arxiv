\subsection{Interpolation with Rotfeld-Inequality}\label{intp3rf}
We now adapt the previous proof to work with a Rotfeld-Inequality instead of
Ky-Fan.
\begin{thm}[Rotfeld-Inequality]\label{rfineq3}
  Let $A$, $B$ be selfadjoint operators which are semi-bound from below.
  Then for $\gamma > 0$ and $\vartheta_0,\vartheta_1\in(0,1)$ it holds that
  \eq{\Tr(A+B+C)^\gamma_- \le \vartheta_0^{-\gamma}\Tr(A)^\gamma_-
        + (1-\vartheta_0)^{-\gamma}\vartheta_1^{-\gamma}\Tr(B)^\gamma_-
        + (1-\vartheta_0)^{-\gamma}(1-\vartheta_1)^{-\gamma}\Tr(C)^\gamma_-.}
  For $\gamma\in(0,1)$ it holds that
  \eq{\Tr(A+B+C)^\gamma_- \le \Tr(A)^\gamma_- + \Tr(B)^\gamma_- + \Tr(C)^\gamma_-.}
  For $\gamma > 1$ it holds that
  \eq{\Tr(A+B+C)^\gamma_- \le \vartheta_0^{-\gamma+1}\Tr(A)^\gamma_-
        + (1-\vartheta_0)^{-\gamma+1}\vartheta_1^{-\gamma+1}\Tr(B)^\gamma_-
        + (1-\vartheta_0)^{-\gamma+1}(1-\vartheta_1)^{-\gamma+1}\Tr(C)^\gamma_-.}
  By taking the limit to $\gamma=1$ in either one of the equations one yields the same estimate
  \eq{\Tr(A+B+C)_- \le \Tr(A)_- + \Tr(B)_- + \Tr(C)_-.}
  So we can ``unify'' these to a best estimate of
  \eq{\Tr(A+B+C)^\gamma_- \le a(\gamma,\vartheta_0,\vartheta_1)\Tr(A)^\gamma_- + b(\gamma,\vartheta_0,\vartheta_1)\Tr(B)^\gamma_-
        + c(\gamma,\vartheta_0,\vartheta_1)\Tr(C)^\gamma_-}
  with \eq{a(\gamma,\vartheta_0,\vartheta_1) = \begin{cases} 1 & \gamma \le 1 \\ \vartheta_0^{-\gamma + 1} & \gamma > 1\end{cases},
    \qquad &b(\gamma,\vartheta_0,\vartheta_1) = \begin{cases} 1 & \gamma \le 1 \\ (1-\vartheta_0)^{-\gamma + 1}\vartheta_1^{-\gamma + 1} & \gamma > 1\end{cases},\\
      \qquad &c(\gamma,\vartheta_0,\vartheta_1) = \begin{cases} 1 & \gamma \le 1 \\ (1-\vartheta_0)^{-\gamma + 1}(1-\vartheta_1)^{-\gamma + 1} & \gamma > 1\end{cases}}
  and $\vartheta_0,\vartheta_1\in(0,1)$.
  \begin{proof}
    Apply Thm \ref{rfineq} twice.
  \end{proof}
\end{thm}
\begin{thm}
  We have
  \eq{\sum_{k}\abs{E_k(H)}^\gamma\le L_{\gamma,d}\int_{\R^d}\abs{V}^{\gamma+\kappa}\d x}
  with $\kappa=\frac{d}{2}$ and
  \eq{
    L_{\gamma,d} \le
    \frac{\Theta(\eta, \xi, p_0 + \kappa, p_1 + \kappa, p_2 + \kappa)}{\Theta(\eta, \xi, p_0, p_1, p_2)}
    a(\gamma)^{1-\eta -\xi}b(\gamma)^\eta c(\gamma)^\xi
    \theta_0^{-\kappa(1 - \eta - \xi)}\theta_1^{-\kappa\eta}(1-\theta_0-\theta_1)^{-\kappa\xi}
    L_{p_0, d}^{1-\eta-\xi}L_{p_1, d}^\eta L_{p_2, d}^\xi
  }
  where $\gamma = p_0+\eta(p_1-p_0)+\xi(p_2-p_0)$
  and $a(\gamma),b(\gamma),c(\gamma)$ are a choice of constants $a(\gamma,\vartheta_0,\vartheta_1)$,
  $b(\gamma,\vartheta_0,\vartheta_1)$, $c(\gamma,\vartheta_0,\vartheta_1)$
  for arbitrary but fixed $\vartheta_0$, $\vartheta_1$ from Thm \ref{rfineq3}.
  \begin{proof}
    As $K$ is defined as an infimum we gain
    \eq{ K(E_k(H_0), t, p_0, p_1) \le \norm{E_k(H_0)}_{l_{p_0}}^{p_0}}
    by choosing $u_n^{(1)}=u_n^{(2)}=0$ and
    \eq{ K(E_k(H_1), t, p_0, p_1) \le t\norm{E_k(H_1)}_{l_{p_1}}^{p_1}}
    by choosing $u_n^{(0)}=u_n^{(2)}=0$ and
    \eq{ K(E_k(H_2), t, p_0, p_1) \le s\norm{E_k(H_2)}_{l_{p_2}}^{p_2}}
    by choosing $u_n^{(0)}=u_n^{(1)}=0$.
    This allows us to derive the following inequality for arbitrary but
    fixed $\alpha,\beta\in\R$:
    \eq{
      &K(E_k(H_0), t, s, p_0, p_1, p_2) + \alpha K(E_k(H_1), t, s, p_0, p_1, p_2)
      + \beta K(E_k(H_2), t, s, p_0, p_1) \\
      \overset{\text{Def as inf}}&{\le} \norm{E_k(H_0)}_{l_{p_0}}^{p_0} + \alpha t\norm{E_k(H_1)}_{l_{p_1}}^{p_1}
         + \beta s\norm{E_k(H_2)}_{l_{p_2}}^{p_2}\\
      \overset{\text{Thm \ref{abschEHi3}}}&{\le}
        \theta_0^{-\kappa}L_{p_0,d}\norm{V_0}_{L_{p_0+\kappa}}^{p_0+\kappa}
        + \alpha t\theta_1^{-\kappa} L_{p_1,d}\norm{V_1}_{L_{p_1+\kappa}}^{p_1+\kappa}
        + \beta s(1-\theta_0-\theta_1)^{-\kappa} L_{p_2,d}\norm{V_2}_{L_{p_2+\kappa}}^{p_2+\kappa} \\
      &= \theta_0^{-\kappa}L_{p_0,d}\left(\norm{V_0}_{L_{p_0+\kappa}}^{p_0+\kappa}
        + \alpha t \theta_0^\kappa\theta_1^{-\kappa}L_{p_0,d}^{-1}L_{p_1,d}\norm{V_1}_{L_{p_1+\kappa}}^{p_1+\kappa}
        + \beta s \theta_0^\kappa(1-\theta_0-\theta_1)^{-\kappa}L_{p_0,d}^{-1}L_{p_2,d}\norm{V_2}_{L_{p_2+\kappa}}^{p_2+\kappa}\right).
    }
    Taking $\inf_{V=V_0+V_1+V_2}$ over this equation we yield
    \eq{\label{prfK3}
      &\inf_{V=V_0+V_1+V_2} K(E_k(H_0), t, s, p_0, p_1, p_2) + \alpha K(E_k(H_1), t, s, p_0, p_1, p_2)
         + \beta K(E_k(H_2), t, s, p_0, p_1, p_2) \\
      &\le \theta_0^{-\kappa}L_{p_0,d}K(V, \alpha\theta_0^\kappa\theta_1^{-\kappa}L_{p_0,d}^{-1}L_{p_1,d} t,
         \beta\theta_0^\kappa (1 -\theta_0 -\theta_1)^{-\kappa}L_{p_0,d}^{-1}L_{p_2,d}s,
         p_0 + \kappa, p_1 + \kappa, p_2 + \kappa) \\
      \overset{\text{Thm \ref{Phitsfactor}}}&{=}
        \theta_0^{-\kappa(1 - \eta - \xi)}\theta_1^{-\kappa\eta}(1-\theta_0-\theta_1)^{-\kappa\xi}
        \alpha^\eta\beta^\xi L_{p_0, d}^{1-\eta-\xi}L_{p_1, d}^\eta L_{p_2, d}^\xi
        K(V, t, s, p_0 + \kappa, p_1 + \kappa, p_2 + \kappa).
    }
    Since $\Phi_{g,1}$ is monotonic (Thm. \ref{thmfineq3}) it follows that
    \eq{\label{prfPhi3}
      &\Phi_{g,1}\Big[\inf_{V=V_0+V_1+V_2} K(E_k(H_0), t, s, p_0, p_1, p_2)\\
         &\qquad\qquad + \alpha K(E_k(H_1), t, s, p_0, p_1, p_2)
         + \beta K(E_k(H_2), t, s, p_0, p_1, p_2)\Big]\\
      &\le\Phi_{g,1}\left[
        \theta_0^{-\kappa(1 - \eta - \xi)}\theta_1^{-\kappa\eta}(1-\theta_0-\theta_1)^{-\kappa\xi}
        \alpha^\eta\beta^\xi L_{p_0, d}^{1-\eta-\xi}L_{p_1, d}^\eta L_{p_2, d}^\xi
        K(V, t, s, p_0 + \kappa, p_1 + \kappa, p_2 + \kappa)
        \right].
    }
    With the help of this somewhat odd looking inequality we can now derive our main result:
    \eq{
      \norm{E_k(H)}_{l_\gamma}^\gamma &= \norm{E_k(H_0+H_1+H_1)}_{l_\gamma}^\gamma\\
      \thmosal{rfineq3}{\le} 
        a(\gamma) \norm{E_k(H_0)}_{l_\gamma}^\gamma + b(\gamma) \norm{E_k(H_1)}_{l_\gamma}^\gamma + c(\gamma) \norm{E_k(H_2)}_{l_\gamma}^\gamma\\
      \thmosal{eta1eq3}{=} 
        \Theta(\eta, \xi, p_0, p_1, p_2)^{-1}\Big(a(\gamma)\Phi_{g,1}[K(E_k(H_0), t, s, p_0, p_1, p_2)] \\
          &\qquad + b(\gamma)\Phi_{g,1}[K(E_k(H_1), t, s, p_0, p_1, p_2)]
          + c(\gamma)\Phi_{g,1}[K(E_k(H_1), t, s, p_0, p_1, p_2)]\Big) \\
      \thmosal{phig1linear}{=} \Theta(\eta, \xi, p_0, p_1, p_2)^{-1}a(\gamma)\Phi_{g,1}
          \Big[K(E_k(H_0), t, s, p_0, p_1, p_2) \\
          &\qquad + \frac{b(\gamma)}{a(\gamma)} K(E_k(H_1), t, s, p_0, p_1, p_2)
          + \frac{c(\gamma)}{a(\gamma)} K(E_k(H_2), t, s, p_0, p_1, p_2)\Big]
    }
    Taking $\inf_{V=V_0+V_1+V_2}$ over this equation we yield
    \eq{
      \norm{E_k(H)}_{l_\gamma}^\gamma &\le \inf_{V=V_0+V_1+V_2} \Theta(\eta, \xi, p_0, p_1, p_2)^{-1}a(\gamma)\Phi_{g,1}
        \Big[K(E_k(H_0), t, s, p_0, p_1, p_2) \\
        &\qquad + \frac{b(\gamma)}{a(\gamma)} K(E_k(H_1), t, s, p_0, p_1, p_2)
        + \frac{c(\gamma)}{a(\gamma)} K(E_k(H_2), t, s, p_0, p_1, p_2)\Big] \\
      &=  \Theta(\eta, \xi, p_0, p_1, p_2)^{-1}a(\gamma)\Phi_{g,1}
        \Big[\inf_{V=V_0+V_1+V_2} K(E_k(H_0), t, p_0, p_1) \\
        &\qquad + \frac{b(\gamma)}{a(\gamma)} K(E_k(H_1), t, s, p_0, p_1, p_2)
        + \frac{c(\gamma)}{a(\gamma)} K(E_k(H_2), t, s, p_0, p_1, p_2)\Big] \\
      \overset{\eqref{prfPhi3}}&{\le}
        \Theta(\eta, \xi, p_0, p_1, p_2)^{-1}a(\gamma)
        \Phi_{g,1}\Big[
        \theta_0^{-\kappa(1 - \eta - \xi)}\theta_1^{-\kappa\eta}(1-\theta_0-\theta_1)^{-\kappa\xi}
        \left(\frac{b(\gamma)}{a(\gamma)}\right)^\eta \left(\frac{c(\gamma)}{a(\gamma)}\right)^\xi\cdot \\
        &\qquad \cdot L_{p_0, d}^{1-\eta-\xi}L_{p_1, d}^\eta L_{p_2, d}^\xi
        K(V, t, s, p_0 + \kappa, p_1 + \kappa, p_2 + \kappa)\Big]\\
      \overset{\text{Thm \ref{phig1linear}}}&{\le}
        \Theta(\eta, \xi, p_0, p_1, p_2)^{-1}a(\gamma)
        \theta_0^{-\kappa(1 - \eta - \xi)}\theta_1^{-\kappa\eta}(1-\theta_0-\theta_1)^{-\kappa\xi}
        \left(\frac{b(\gamma)}{a(\gamma)}\right)^\eta \left(\frac{c(\gamma)}{a(\gamma)}\right)^\xi\cdot \\
        &\qquad \cdot L_{p_0, d}^{1-\eta-\xi}L_{p_1, d}^\eta L_{p_2, d}^\xi
        \Phi_{g,1}[K(V, t, s, p_0 + \kappa, p_1 + \kappa, p_2 + \kappa)]\\
      \thmosal{eta1eq3}{=} 
        \frac{\Theta(\eta, \xi, p_0 + \kappa, p_1 + \kappa, p_2 + \kappa)}{\Theta(\eta, \xi, p_0, p_1, p_2)}
        a(\gamma)^{1-\eta -\xi}b(\gamma)^\eta c(\gamma)^\xi
        \theta_0^{-\kappa(1 - \eta - \xi)}\theta_1^{-\kappa\eta}(1-\theta_0-\theta_1)^{-\kappa\xi} \,\cdot\\
        &\qquad \cdot L_{p_0, d}^{1-\eta-\xi}L_{p_1, d}^\eta L_{p_2, d}^\xi
        \norm{V}_{L_{\gamma+\kappa}}^{\gamma+\kappa}
    }
  \end{proof}
\end{thm}
\begin{defn}\label{defY3}
  We define
  \eq{Y(\eta, \xi, \gamma) :=& \inf_{a(\gamma), b(\gamma), c(\gamma)\text{ from Thm \ref{rfineq3}}}a(\gamma)^{1-\eta-\xi}b(\gamma)^\eta c(\gamma)^\xi\\
                      =& \inf_{\vartheta_0,\vartheta_1\in (0,1)}a(\gamma,\vartheta_0,\vartheta_1)^{1-\eta-\xi}
                         b(\gamma,\vartheta_0,\vartheta_1)^\eta c(\gamma,\vartheta_0,\vartheta_1)^\xi}
  in order to summarize the influence of Rotfeld-related $a$, $b$ and $c$ terms into a dedicated function.
\end{defn}
\begin{thm}
  To optimize the choice of $\vartheta_0,\vartheta_1$ in Thm. \ref{thmrfint} we calculate
  \eq{
    Y(\eta, \xi, \gamma) = \begin{cases}
      1 & \gamma \le 1 \\
      (1-\eta-\xi)^{(-\gamma+1)(1-\eta-\xi)}\eta^{(-\gamma+1)\eta}\xi^{(-\gamma+1)\xi} & \gamma > 1.
    \end{cases}
  }
  \begin{proof}
    With a quick calculation it can be seen that
    \eq{\inf_{\vartheta_0,\vartheta_1\in (0,1)}
        \vartheta_0^{(-\gamma+1)(1-\eta-\xi)}(1-\vartheta_0)^{(-\gamma+1)(\eta+\xi)}\vartheta_1^{(-\gamma+1)\eta}(1-\vartheta_1)^{(-\gamma+1)\xi}\\
%        = (1-\eta-\xi)^{(-\gamma+1)(1-\eta-\xi)}(\eta+\xi)^{(-\gamma+1)(\eta+\xi)}\frac{\eta^{(-\gamma+1)\eta}\xi^{(-\gamma+1)\xi}}{(\eta+\xi)^{(-\gamma+1)(\eta+\xi)}}
         = (1-\eta-\xi)^{(-\gamma+1)(1-\eta-\xi)}\eta^{(-\gamma+1)\eta}\xi^{(-\gamma+1)\xi}.
    }
  \end{proof}
\end{thm}
\begin{cor}
  By Thms \ref{defY3}, \ref{opttheta3} we obtain
  \eq{L_{\gamma,d} \le
    \frac{\Theta(\eta, \xi, p_0 + \kappa, p_1 + \kappa, p_2 + \kappa)}{\Theta(\eta, \xi, p_0, p_1, p_2)}
    Y(\eta, \gamma) (1- \eta - \xi)^{-\kappa(1-\eta-\xi)}\eta^{-\kappa\eta}\xi^{-\kappa\xi}
    L_{p_0, d}^{1-\eta-\xi}L_{p_1, d}^\eta L_{p_2, d}^\xi.
  }
\end{cor}

\subsubsection{Unified notation for results and plots}
With $C_{\gamma,d}^{(a,b,c), \text{rf3}}$, $\gamma\in (a,c)$ we denote the upper bound that has been
achieved by Rotfeld 3 power interpolation (rf3) with $p_0=a$, $p_1=b$, $p_2=c$, $L_{{p_i,d}} =
C_{p_i,d}^\text{lit}L_{p_i,d}^\text{cl}$ ($i=0,1$).

\subsubsection{Numerical Results}
In the following results $\eta$ has been chosen in the bound of \ref{etachoice}
so that $L_{\gamma,1}$ becomes minimal. The resulting $\eta$ of the minimization can
be seen in Fig. \ref{plot:interp-rf-res3p-fact-eta}.
\nopagebreak
\plotinc{s1d3p_rf_total.tex}
{Numeric evaluation of the achieved constants for $d=1$, $p_0=\frac{1}{2}$, $p_1=1$, $p_2=\frac{3}{2}$,
$C_{\frac{1}{2},1}=2$, $C_{1,1}\le 1.456$, $C_{\frac{3}{2},1}=1$.}
{plot:interp-rf-res3p-total}
\plotinc{s1d3p_rf_fact_eta.tex}
{Numeric evaluation of the optimal $\eta$ for $d=1$, $p_0=\frac{1}{2}$, $p_1=1$, $p_2=\frac{3}{2}$,
$C_{\frac{1}{2},1}=2$, $C_{1,1}\le 1.456$, $C_{\frac{3}{2},1}=1$.}
{plot:interp-rf-res3p-fact-eta}

It seems that $\eta$ is either very close to $0=\eta_\text{min}$ or to
$\eta_\text{max}$. If the minimum is at very small distance from those points
or if the small distance is the result of imprecise numerics (since for
$\eta\to 0$ or $\eta\to\eta_\text{max}$ both $\Theta$ terms diverge this is
hard to calculate).

\plotinc{s1d3p_rf_fact.tex}
{Numeric evaluation of the constituting factors for $d=1$, $p_0=\frac{1}{2}$, $p_1=1$, $p_2=\frac{3}{2}$,
$C_{\frac{1}{2},1}=2$, $C_{1,1}\le 1.456$, $C_{\frac{3}{2},1}=1$.}
{plot:interp-rf-res3p-fact}

\paragraph{Comparison with 2 power interpolation results}\,
\plotinc{s1d3p_rf_total_comp.tex}
{Numeric comparison of the constituting factors for 3 and 2 point interpolation for $d=1$, $p_0=\frac{1}{2}$, ($p_1=1$), $p_2=\frac{3}{2}$,
$C_{\frac{1}{2},1}=2$, $C_{1,1}\le 1.456$, $C_{\frac{3}{2},1}=1$.}
{plot:interp-rf-res3p-total-comp}
As seen in Fig. \ref{plot:interp-rf-res3p-total-comp} for a wide range of
powers the 3-point version seems to either approximate one of the 3 2-point
approximations from above or is between the two of them that exist at the same
$\gamma$.\\

We note that the effect of not approximating the minimum in the interval
$(1,1+\delta)$ is not due to $Y\neq 1$ in that interval, the same effect occurs
if the optimization is performed with $Y=1$ everywhere.
\plotinc{s1d3p_rf_comp.tex}
{Numeric comparison of the constituting factors for 3 and 2 point interpolation for $d=1$, $p_0=\frac{1}{2}$, ($p_1=1$), $p_2=\frac{3}{2}$,
$C_{\frac{1}{2},1}=2$, $C_{1,1}\le 1.456$, $C_{\frac{3}{2},1}=1$.}
{plot:interp-rf-res3p-comp}
In contrast to the $M$-Term of Ky-Fan that gets drastically worse in the three
point variant the $Y$-Term of Rotfeld stays relatively tame as seen
in Fig. \ref{plot:interp-rf-res3p-comp}.
\plotinc{s1d3p_rf_comp_2.tex}
{Numeric comparison of the constituting factors for 3 and 2 point interpolation for $d=1$, $p_0=\frac{1}{2}$, ($p_1=1$), $p_2=\frac{3}{2}$,
$C_{\frac{1}{2},1}=2$, $C_{1,1}\le 1.456$, $C_{\frac{3}{2},1}=1$.}
{plot:interp-rf-res3p-comp-2}


The overall conclusion from the numerical results is that for our example of
interest (behaviour of $L_\gamma,1$) the 3-point interpolation is
mostly just a worse version of the 3 separate 2-point interpolations.
