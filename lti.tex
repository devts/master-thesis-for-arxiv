\chapter{Lieb Thirring inequalities}
\label{ch:lti}
\section{Theory}
\label{sec:lti_theory}
In this section we will introduce the Lieb-Thirring inequalities and give an
overview of relevant results in literature.
\begin{thm}[Form of the Schrödinger Operator, \cite{weidlbook}]\label{formschroedinger}
  Let $V=V_+ - V_-$, $V_\pm(x)=\max(\pm V(x),0)$ with $V_+\in L_{1,\text{loc}}$ and $V_- \in L_p$ with
  \eq{\mcases{p\ge 1 & d = 1\\ p > 1 & d = 2 \\ p \ge \frac{d}{2} & d \ge 3}.}
  Then the quadratic Form 
  \eq{h[u] = \int_{\R^d} \abs{\nabla u}^2 + V\abs{u}^2 \d x}
  is closed and semibounded from below on its domain
  \eq{D(h)=H^1(\R^d)\cap L_2(\R^d, V_+\d x).}
\end{thm}
\begin{defn}[Schrödinger operator, \cite{weidlbook}]\label{defSchroedinger}
  Thm. \ref{saoplsquadform} states that the form $h$ defined in Thm.
  \ref{formschroedinger} has a unique self-adjoint associated operator $H$,
  since $h$ is both semibounded from below and closed.
  We denote this operator by $H=-\laplace + V$ even though it it strictly
  speaking not defined by operator addition since it is obviously designed an
  extension of such operators.
\end{defn}
\begin{thm}[Essential spectrum of the Schrödinger operator, \cite{weidlbook}]
  The essential spectrum of the operator $H$ as defined in
  Def. \ref{defSchroedinger} fulfills
  \eq{\sigma_{\text{ess}}(H) = \sigma_{\text{ess}}(-\laplace + V)
      = \sigma_{\text{ess}}(-\laplace + V_+) \subset [0,\infty).}
\end{thm}
\begin{rem}[Notation of negative eigenvalues]
  Let $H=-\laplace + V$ be defined as above.
  Since $H$ is semibounded from below and $\sigma_{\text{ess}}(H) \subset
  [0,\infty)$ the negative spectrum is discrete and the eigenvalues can only
  accumulate at zero. Thus we can define $E_n=E_n(H)$ to be the monotonically
  increasing sequence of negative eigenvalues of $H$
  ($\lambda_0(H)=\inf\sigma(A)$ being the lowest).
  Each eigenvalue shall appear as often as its multiplicity.
  (I.e. the union $\sigma_d(H)\cap(-\infty, 0) =\cup_n E_n$ is disjoint
   if and only if all the eigenvalues are non-degenerate.)
\end{rem}
\begin{defn}[Lieb Thirring inequality, \cite{lt1976}]
  We can now use the previous two definitions to define the Lieb Thirring
  constants $L_{\gamma, d}$ as the minimal constant in the Lieb Thirring
  inequality
  \eq{\sum_{n}\abs{E_n(H)}^\gamma \le L_{\gamma, d}\int_{\R^d} V_-^{\gamma+\frac{d}{2}}\d x
  .\label{LTeq}}
  This inequality was introduced by Lieb and Thirring in \cite{lt1976} who used
  it to prove a dual inequality for $\gamma=1$ (see Thm \ref{duallti})
  that is related to the stability of matter (see Rem \ref{stabmtr}).
\end{defn}
\begin{cor}
  The choice of $V$ in \eqref{LTeq} can be restricted to $V\le 0$ without
  changing the value of $L_{\gamma, d}$ since
  \eq{E_n(-\laplace - V_- + V_+) \ge E_n(-\laplace - V_-),}
  which means the opposite inequality holds for the absolute values and thus for
  the sum on the left hand side of \eqref{LTeq} while the right hand side of
  \eqref{LTeq} does by definition not change.
  This can be useful since for negative $V$ we have
  $D(h)=H^1(\R^d)$ and $\sigma_\text{ess}(H) = [0,\infty)$.
\end{cor}
\begin{thm}[Dual LTI, \cite{frankrecent}]\label{duallti}
  Let $K_d$ be the best constant such that the inequality
  \eq{
    \sum_{n=1}^N\int_{\R^d}\abs{\nabla u_n}^2 \d x
    \ge K_d \int_{\R^d} \left(\sum_{n=1}^N\abs{u_n}^2\right)^{1+\frac{2}{d}}\d x
  }
  holds for all $N\in\N$, $(u_n)_{i=1}^N\subset H^1(\R^d)$, $\fa{i,j}(u_i,u_j)=\delta_{i,j}$.\\
  Then $K_d$ and $L_{1,d}$ are related by
  \eq{\left(\left(1+\frac{d}{2}\right)L_{1,d}\right)^{1+\frac{2}{d}}
      \left(\left(1+\frac{2}{d}\right)K_d\right)^{1+\frac{d}{2}} = 1.}
\end{thm}
\begin{rem}[Stability of matter, \cite{lt1975}]\label{stabmtr}
  Let \eq{H_N = \sum_{i=1}^N - \laplace_i - \sum_{i=1}^N\sum_{j=1}^M Z_k\abs{x_i-R_k}^{-1}
                + \sum_{i>j} \abs{x_i-x_j}^{-1} + \sum_{k>m}\frac{Z_kZ_m}{\abs{{R_k-R_m}}}}
  be the Hamiltonian of a system with $N$ electrons and $M$ nuclei of charge $Z_i$.
  Then the energy of the system is bounded from below by
  \eq{(\phi, H_n \phi) \ge - 2.08 N \left(1+\left(\sum_{j=1}^M Z_j^\frac{7}{3}N^{-1}\right)^\frac{1}{2}\right)^2.}
\end{rem}
The proof of the Stability of matter is both the origin of the Lieb-Thirring
inequalities and their most central application.
For further studies on the stability of matter and proofs thereof we refer the
interested reader to \cite{liebstabmtr}.
\begin{thm}[Existence]
  $L_{\gamma, d}$ exists and is finite if and only if
  \eq{\label{gammalti}\mcases{
    \gamma \ge \frac{1}{2} & d = 1 \\
    \gamma > 0 & d = 2 \\
    \gamma \ge 0 &  d\ge 3
  }}
  The edge case of $\gamma=\frac{1}{2}$, $d=1$ was settled by Weidl in \cite{weidlinterpolation}.
  The edge case of $\gamma=0$, $d\ge 3$ was settled independently by Cwinkel \cite{cwikelL0d},
  Lieb \cite{liebL0d} and Rosenblum \cite{rosenL0d} and is also known as CLR inequality.
  The remaining cases were directly settled by Lieb and Thirring in \cite{lt1976}.
\end{thm}
\begin{thm}[Classical constant, \cite{lt1976}]
  For $V\in L_{\gamma+\frac{d}{2}}$ with $\gamma$ as in \eqref{gammalti}
  it holds that in the semi-classical limit (``$\hbar\to 0$'')
  \eq{\lim_{\alpha\to\infty} \alpha^{-\gamma+\frac{d}{2}}\Tr(-\laplace + \alpha V)_-^\gamma
      = L_{\gamma, d}^\text{cl}\int_{\R^d} V_-^{\gamma+\frac{d}{2}}\d x}
  with the classical constant
  \eq{L_{\gamma, d}^\text{cl} = (4\pi)^{-\frac{d}{2}}\frac{\Gamma(\gamma + 1)}{\Gamma(\gamma + 1 + \frac{d}{2})}.}
  \plotincxx{plot_lcl1.tex}{plot_lcl2.tex}{plot_lcl3.tex}
  {Plot of $L_{\gamma, d}^\text{cl}$, $d=1,2,3$}
  {plot:lcl}
\end{thm}
\begin{thm}[Monotonicity, \cite{Lmonotonic}]\label{LLclmonotonic}
  $\sds\frac{L_{\gamma, d}}{L_{\gamma, d}^\text{cl}}$ is monotonically decreasing in $\gamma$ for all $d\ge 1$.\\
  This important theorem was fist proven by Aizenmann and Lieb in \cite{Lmonotonic}.
\end{thm}
\begin{defn}
  Since thanks to the monotonicty property most problems are
  more natural to express in ``units'' of $L_{\gamma,d}^\text{cl}$
  we introduce
  \eq{C_{\gamma,d} := L_{\gamma,d} / L_{\gamma,d}^\text{cl}.}
\end{defn}
\begin{thm}[Single particle constant, \cite{lt1976}]
  A somewhat similar problem concerns the optimum constant if we only consider
  the lowest eigenvalue in the left hand side of the Lieb Thirring Inequality.
  \eq{\abs{E_1(H)}^\gamma \le L_{\gamma, d}^{(1)}\int_{\R^d} V_-^{\gamma+\frac{d}{2}}\d x}
  In dimension $d=1$ this problem has been analytically solved with the solution
  \eq{L_{\gamma, d}^{(1)} = \frac{1}{\sqrt{\pi}}
    \frac{\Gamma(\gamma + 1)(\gamma - \frac{1}{2})^{\gamma - \frac{1}{2}}}
      {\Gamma(\gamma + \frac{1}{2})(\gamma + \frac{1}{2})^{\gamma + \frac{1}{2}}}.
  }
  \plotinc{plot_lsingle1.tex}
  {Plot of $L_{\gamma, d}^{(1)}$ and $L_{\gamma, d}^\text{cl}$, $d=1$}
  {plot:lsingle1}
  In higher dimensions no explicit anlytic expression is known but numerical
  results are available.
\end{thm}
\begin{cor}
  $L_{\gamma, d} \ge \max\m{L_{\gamma,d}^{(1)},L_{\gamma, d}^\text{cl}}$
\end{cor}
Because of their importance we give a short introduction to operator valued LTI's:
\begin{defn}[Operator valued LTI, \cite{laptevweidlop}, \cite{hlwlift2}]
  Let $G$ be a Hilbert Space and
  \eq{V:\R^d\to B(G)\qquad\text{with}\qquad \norm{V(\,\cdot\,)}_{B(G)}\in L_p(\R^d)}
  where like in the scalar case the range for $p$ is
  \eq{\mcases{p\ge 1 & d = 1\\ p > 1 & d = 2 \\ p \ge \frac{d}{2} & d \ge 3}.}
  So rather than $V(x)$ being a scalar as we would have in the conventional case we now have an operator:
  \eq{B(G)\ni V(x):G\to G.}
  We can still use $V$ as a ``multiplication'' operator
  by defining it as the operator that belongs to the quadratic form
  \eq{v[u] := \int_{\R^d}(V(x)(u(x)), u(x))_G \d x.}
  To illustrate how this operator works, consider the case
  $\norm{V(\,\cdot\,)}_{B(G)} \in L_\infty(\R^d)$: Then we can write
  \eq{V:L_2(\R^d, G) &\to L_2(\R^d, G)\\
      u &\mapsto Vu, (Vu)(x)=V(x)(u(x))}
  which is similar to the common multiplication operator,
  just that we now apply the operator $V(x)$ to $u(x)$ instead of multiplying with its value.
  This allows us to define
  \eq{H= -\laplace + V:H^1(\R^d, G)\to L_2(\R^d, G)}
  again as the Operator that belongs to the quadratic form
  \eq{h[u] := \int_{\R^d} \sum_{j=1}^d \norm{\partial_{x_j} u}^2_G \d x + v[u]}
  and pose the question if an operator valued LTI holds:
  \eq{\Tr(-\laplace + V)_-^\gamma \le L_{\gamma,d}^\text{op} \int_{\R^d}\Tr V_-^{\gamma+\frac{1}{2}}\d x}
  Obviously $L_{\gamma,d} \le L_{\gamma, d}^\text{op}$ since $G=\C$ yields the classic LTI.\\

  The monotonicity property that holds for the classical LTI also holds for the operator
  valued analogon with $L_{\gamma, d}^\text{op,cl} = L_{\gamma, d}^\text{cl}$ and we define
  in analogous fashion $C_{\gamma, d}^\text{op}:= L_{\gamma, d}^\text{op}/L_{\gamma, d}^\text{cl}$.\\
\end{defn}
\begin{thm}[Laptev-Weidl lifting, \cite{laptevweidlop}, \cite{hlwlift2}, \cite{hundertmarkop}]
  What makes this Operator valued LTI so interesting is that it allows us to
  ``lift'' the results in dimension in the following sense:\\
  This construction allows us to do a ``split in dimension'' with using $G=H^1(\R^{d-n}, \C)$
  such that we can regard a function $u\in H^1(\R^d, \C)$ as $u\in H^1(\R^n, H^2(\R^{d-n}, \C))$
  and split the Laplace with placing the last $d-n$ dimensions as part of the potential:
  \eq{-\laplace + V = -\sum_{i=1}^n \partial_{x_i}^2 + W, \quad W = -\sum_{i=n+1}^d\partial^2_{x_i} + V}
  which ultimately leads to
  \eq{C_{\gamma, d}^\text{op} \le C_{\gamma, n}^\text{op} C_{\gamma+\frac{n}{2}, d-n}^\text{op}
  \label{Clift}}
  for $n<d\in\N$, $\gamma \ge \frac{1}{2}$ in $d\le 2$ and $\gamma \ge 0$ in $d\ge 3$.
  This was proven for $n=1$ and $\gamma\ge \frac{1}{2}$ in \cite{hlwlift2}
  and later extended by Hundertmark in \cite{hundertmarkop} to this broader formulation.
  For details we refer to these publications.
  \label{liftinggen}
\end{thm}
\begin{cor}
  This leads to
  \eq{C_{\gamma,d}\le C_{\gamma,1}^\text{op}
    \qquad\text{ for $1\le\gamma\le\frac{3}{2}$, $d\ge 1$}}
  and
  \eq{C_{\gamma,d}\le C_{\gamma,1}^\text{op}C_{\gamma+\frac{1}{2},1}^\text{op}
    \qquad\text{for $\frac{1}{2}\le\gamma < 1$, $d\ge 1$}}
  which were proven by
  Hundertmark, Laptev and Weidl in \cite{hlwlift2} 
  and can be derived from Thm. \ref{liftinggen} with $C_{\gamma,d}\le C_{\gamma,d}^\text{op}$
  and $C_{\gamma,d}^{op}=1$ for $\gamma\ge\frac{3}{2}$.
  \label{prevreslifting2}
\end{cor}
\begin{thm}[Previous results]\label{prevres}
  \begin{enumerate}
  \item For $\gamma=0$ the best known constants are achieved in the scalar case
        by Lieb in \cite{liebL0d} for $d=3,4$ and by Hundertmark, Kunstmann, Ried
        and Vugalter in \cite{hkrvL0d} for $d\ge 5$ in the scalar case as well
        as $d\ge 3$ in the operator valued case.
        See Thm. \ref{litbound} for the concrete numbers.
  \item $L_{\gamma, 1} = L_{\gamma, 1}^\text{cl}$ for $\gamma = \frac{3+2n}{2}$, $\N\ni n\ge 0$
        was proven by Lieb and Thirring in \cite{lt1976}.
        As noted in \cite{Lmonotonic}, this result can be extended to
        $\gamma \ge\frac{3}{2}$ thanks to Thm. \ref{LLclmonotonic} (which is due
        to the same paper).
  \item $L_{\gamma, d} = L_{\gamma, d}^\text{cl}$ for $\gamma\ge\frac{3}{2}$
        was established by generalizing the previous result to higher dimensions
        with an operator valued potential method in \cite{laptevweidlop}. \label{prevreslifting1}
  \item $L_{\frac{1}{2},1}<4.02L_{\frac{1}{2},1}^\text{cl}$
        was proven by Weidl in \cite{weidlinterpolation}.
  \item $L_{\frac{1}{2},1}=2L_{\frac{1}{2},1}^\text{cl}$
        was later proven by Hundertmark, Lieb and Thomas in \cite{L121sharp}.
  \item The best currently known bound for $L_{1,d}$, $L_{1,d} \le 1.456L_{1,d}^\text{cl}$
        was proven by Frank, Hundertmark and Rex in \cite{fhrL11}.
        The proof is made for $L_{1,1}^\text{op}$,
        from which the result follows with Cor. \ref{prevreslifting2}.
        This allows one together with $C_{\frac{1}{2},1}^\text{op} = 2$
        and Cor. \ref{prevreslifting2} to derive $C_{\frac{1}{2}, d}\le 2\cdot 1.456 = 2.912$ for $d\ge 2$.
  \end{enumerate}
\end{thm}
\begin{rem}[Known bounds]\label{litbound}
  Using the monotonicity and the previous results listed in Thm \ref{prevres} we yield:
  For the range $0\le \gamma < \frac{1}{2}$ for dimension $d\ge 3$
  \eq{L_{\gamma, d} \le  L_{\gamma, d}^\text{cl}\cdot
    \begin{cases}
      6.86924 & d = 3 \\
      6.03398 & d = 4 \\
      5.95405 & d \ge 5 \\
    \end{cases}
  }
  in the scalar case and
  \eq{L_{\gamma, d}^\text{op} \le  L_{\gamma, d}^\text{cl}\cdot
    \begin{cases}
      7.55151 & d = 3 \\
      6.32791 & d = 4 \\
      5.95405 & d \ge 5 \\
    \end{cases}
  }
  in the operator valued case.
  For the range $\frac{1}{2} \le \gamma < 1$ we have
  \eq{L_{\gamma, d} \le  L_{\gamma, d}^\text{cl}\cdot
    \begin{cases}
      2 & d = 1 \\
      2.912 & d \ge 2 \\
    \end{cases}
  }
  in both the scalar and the operator valued case.
  For the range $1 \le \gamma < \frac{3}{2}$ we have
  \eq{L_{\gamma, d} \le  L_{\gamma, d}^\text{cl}\cdot 1.456}
  in both the scalar and the operator valued case
  and for $\gamma \ge \frac{3}{2}$ we have
  \eq{L_{\gamma, d} = L_{\gamma, d}^\text{cl}}
  also in both the scalar and the operator valued case.
\end{rem}
\begin{defn}\label{defClit}
  We denote the results from Thm \ref{litbound} in each dimension as $C_{\gamma,d}^\text{lit}$, i.e.
  \eq{C_{\gamma,1}^\text{lit, op} = C_{\gamma,1}^\text{lit}
      :=\mcases{2 & \gamma\in [\sfrac{1}{2},1)\\1.456 & \gamma\in [1,\sfrac{3}{2})\\ 1 & \gamma\in [\sfrac{3}{2},\infty)}}
  \eq{C_{\gamma,2}^\text{lit, op} = C_{\gamma,2}^\text{lit}
      :=\mcases{2.912 & \gamma\in [\sfrac{1}{2},1))\\ 1.456 & \gamma\in [1,\sfrac{3}{2})\\ 1 & \gamma\in [\sfrac{3}{2},\infty)}}
  \eq{C_{\gamma,3}^\text{lit}:=\mcases{6.86924 & \gamma\in [0,\sfrac{1}{2})\\ 2.912 & \gamma\in [\sfrac{1}{2},1)\\
       1.456 & \gamma\in [1,\sfrac{3}{2})\\ 1 & \gamma\in [\sfrac{3}{2},\infty)}
  \qquad C_{\gamma,3}^\text{lit, op}:=\mcases{7.55151 & \gamma\in [0,\sfrac{1}{2})\\ 2.912 & \gamma\in [\sfrac{1}{2},1)\\
       1.456 & \gamma\in [1,\sfrac{3}{2})\\ 1 & \gamma\in [\sfrac{3}{2},\infty)}}
  \eq{C_{\gamma,4}^\text{lit}:=\mcases{6.03398 & \gamma\in [0,\sfrac{1}{2})\\ 2.912 & \gamma\in [\sfrac{1}{2},1)\\
       1.456 & \gamma\in [1,\sfrac{3}{2})\\ 1 & \gamma\in [\sfrac{3}{2},\infty)}
  \qquad C_{\gamma,4}^\text{lit, op}:=\mcases{6.32791 & \gamma\in [0,\sfrac{1}{2})\\ 2.912 & \gamma\in [\sfrac{1}{2},1)\\
       1.456 & \gamma\in [1,\sfrac{3}{2})\\ 1 & \gamma\in [\sfrac{3}{2},\infty)}}
  and so on.
\end{defn}
\input{coulomb.tex}
