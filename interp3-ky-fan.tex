\subsection{Interpolation with Ky-Fan}\label{intp3kf}
\begin{thm}[Ky-Fan inequality]\label{ky-fan3}
  For $H_0$ and $H_1$ as defined in Thm \ref{zerlH3} it holds that
  \eq{\abs{E_{n+l+k-2}(H)}\le\abs{E_n(H_0)}+\abs{E_l(H_1)}+\abs{E_k(H_2)}.}
  \begin{proof}
    Apply Thm \ref{ky-fan} twice with $m=l+k-1$:
    \eq{\abs{E_{n+l+k-2}(H)} = \abs{E_{n+m-1}(H)} \le \abs{E_n(H_0)}+\abs{E_m(H_1+H_2)}
        \le \abs{E_n(H_0)}+\abs{E_l(H_1)}+\abs{E_k(H_2)}.}
  \end{proof}
\end{thm}
\begin{thm}\label{thmkfint3}
  We have
  \eq{\sum_{k}\abs{E_k(H)}^\gamma\le L_{\gamma,d}\int_{\R^d}\abs{V}^{\gamma+\kappa}\d x}
  with $\kappa=\frac{d}{2}$ and
  \eq{L_{\gamma,d} \le
    \frac{\Theta(\eta, \xi, p_0+\kappa, p_1+\kappa, p_2+\kappa)}{\Theta(\eta, \xi, p_0, p_1, p_2)}
    L_{p_0,d}^{1-\eta-\xi}L_{p_1,d}^\eta L_{p_2,d}^\xi
    (1+2N^{-1})^{1-\eta-\xi}(2+N)^{\eta+\xi}\,\cdot\qquad\\\cdot\,
    \theta_0^{-\kappa+\kappa\eta+\kappa\xi}\theta_1^{-\kappa\eta}(1-(\theta_0+\theta_1))^{-\kappa\xi}
  }
  where $\gamma = p_0+\eta(p_1-p_0)+\xi(p_2-p_0)$.
  \begin{proof}
    For $\N\ni N\ge 1$ we define
    \eq{a^{(i)}_k:=\abs{E_{l_i(k)}(H_0)}, \qquad l_0(k) &= k + 2 - l_1(k) - l_2(k), \quad
        l_1(k)=1+\floor{\frac{k+1}{N+2}}, \quad l_2(k)=1+\floor{\frac{k}{N+2}}.}
    Then we can use Ky-Fan to derive
    \eq{\abs{E_k(H)} \stackrel{\ref{ky-fan3}}{\le} a^{(0)}_k + a^{(1)}_k + a^{(2)}_k} 
    %Ex. N = 2
    %1 2 3 4 5 6 7 8 k
    %1 1 1 2 2 2 2 3 l_2
    %1 1 2 2 2 2 3 3 l_1
    %1 2 2 2 3 4 4 4 l_0
    and use the definition of the sequences above to derive the following two estimates:
    \eq{\norm{(a^{(i)}_k)}_{l_i}^{p_i} \le (2+N)\sum_k \abs{E_k(H_i)}^{p_i}
        &\qquad \parbox[t]{25em}{for $i=1,2$ since every index is hit $N+2$ times due to the floor}\\[-3em]
        \norm{(a^{(0)}_k)}_{l_1}^{p_1} \le (1+2N^{-1})\sum_k \abs{E_k(H_1)}^{p_1} &\qquad
        \parbox[t]{25em}{since every index is hit once except for indices that are multiples of N
              who are hit three times (Index $zN$ is hit from $k= zN, z(N+1), z(N+2)$) whose weight is evenly redistributed as $2N^{-1}$
              on the other indices which is possible since the eigenvalues are ordered decreasing in magnitude.}}
    Using these inequalities we gain
    \eq{K((E_k(H)), t, p_0, p_1) &\le K((a^{(0)}_k + a^{(2)}_k + a^{(2)}_k), t, p_0, p_1) \\
            \intertext{since $K$ is defines as an infimum,
               choosing the particular split $u_k^{(i)}=a^{(i)}_k$ in Def. \ref{defK3} we gain}
        &\le \norm{a^{(0)}_k}_{l_{p_0}}^{p_0} + t\norm{a^{(1)}_k}_{l_{p_1}}^{p_1} + s\norm{a^{(1)}_k}_{l_{p_2}}^{p_2}\\
        &\le (1+2N^{-1})\sum_k \abs{E_k(H_0)}^{p_0} + t(2+N)\sum_k \abs{E_k(H_1)}^{p_1} + s(2+N)\sum_k \abs{E_k(H_2)}^{p_2}\\
        \thmosal{abschEHi3}{\le} (1+2N^{-1})\theta_0^{-\kappa}L_{p_0,d}\norm{V_0}_{L_{p_0+\kappa}}^{p_0+\kappa} \\
             &\quad + t(2+N)\theta_1^{-\kappa} L_{p_1,d}\norm{V_1}_{L_{p_1+\kappa}}^{p_1+\kappa}
             + s(2+N)(1-(\theta_0+\theta_1))^{-\kappa} L_{p_2,d}\norm{V_2}_{L_{p_2+\kappa}}^{p_2+\kappa} \\
        &= (1+2N^{-1})\theta_0^{-\kappa}L_{p_0,d}\Bigg(\norm{V_0}_{L_{p_0+\kappa}}^{p_0+\kappa}
             + t\frac{(2+N)\theta_1^{-\kappa} L_{p_1,d}}{(1+2N^{-1})\theta_0^{-\kappa}L_{p_0,d}}
               \norm{V_1}_{L_{p_1+\kappa}}^{p_1+\kappa} \\
             &\quad + s\frac{(2+N)(1-(\theta_0+\theta_1))^{-\kappa} L_{p_2,d}}{(1+2N^{-1})\theta_0^{-\kappa}L_{p_0,d}}
               \norm{V_2}_{L_{p_2+\kappa}}^{p_2+\kappa}\Bigg).
    }
    Taking the infimum over all possible decompositions $V_0$ and $V_1$ we thus obtain
    \eq{K((E_k(H)), &t, p_0, p_1) \le (1+2N^{-1})\theta_0^{-\kappa}L_{p_0,d} \,\,\cdot \\
        &\cdot\,\, K\left(V, t\frac{(2+N)\theta_1^{-\kappa} L_{p_1,d}}{(1+2N^{-1})\theta_0^{-\kappa}L_{p_0,d}},
                             s\frac{(2+N)(1-(\theta_0+\theta_1))^{-\kappa} L_{p_2,d}}{(1+2N^{-1})\theta_0^{-\kappa}L_{p_0,d}},
                             p_0+\kappa, p_1+\kappa, p_2+\kappa\right).}
    Applying $\Phi_{g, 1}$ (which is possible thanks to Thm \ref{thmfineq3})
    to the last inequality we yield with Thm \ref{eta1eq3} and Thm \ref{Phitsfactor}
    \eq{&\Theta(\eta, \xi, p_0, p_1, p_2) \norm{(E_k(H))}_{l_\gamma}^\gamma
        \le (1+2N^{-1})\theta_0^{-\kappa}L_{p_0,d} \,\,\cdot\\
        &\,\,\cdot\,\,\left(\frac{(2+N)\theta_1^{-\kappa} L_{p_1,d}}{(1+2N^{-1})\theta_0^{-\kappa}L_{p_0,d}}\right)^\eta
        \left(\frac{(2+N)(1-(\theta_0+\theta_1))^{-\kappa} L_{p_2,d}}{(1+2N^{-1})\theta_0^{-\kappa}L_{p_0,d}}\right)^\xi
        \Theta(\eta, \xi, p_0+\kappa, p_1+\kappa, p_2+\kappa)\norm{V}_{L_{\gamma+\kappa}}^{\gamma+\kappa}.
    }
    Which simplifies to our desired expression
    \eq{\norm{(E_k(H))}_{l_\gamma}^\gamma&\le 
        \frac{\Theta(\eta, \xi, p_0+\kappa, p_1+\kappa, p_2+\kappa)}{\Theta(\eta, \xi, p_0, p_1, p_2)}
        L_{p_0,d}^{1-\eta-\xi}L_{p_1,d}^\eta L_{p_2,d}^\xi \,\,\cdot\\
        &\quad \cdot\,\,(1+2N^{-1})^{1-\eta-\xi}(2+N)^{\eta+\xi}
        \theta_0^{-\kappa+\kappa\eta+\kappa\xi}\theta_1^{-\kappa\eta}(1-(\theta_0+\theta_1))^{-\kappa\xi}
        \norm{V}_{L_{\gamma+\kappa}}^{\gamma+\kappa}.
    }
  \end{proof}
\end{thm}
\begin{defn}
  We define
  \eq{M(\eta, \xi) := \inf_{N\ge 1}(1+2N^{-1})^{1-\eta-\xi}(2+N)^{\eta+\xi}}
  in order to summarize the influence of $N$-related terms into a dedicated function.
\end{defn}
\begin{thm}\label{opttheta3}
  To optimize the choice of $\theta_0,\theta_1$ in Thm. \ref{thmkfint3} we calculate
  \eq{
    \inf_{\substack{\theta_0, \theta_1\in (0,1) \\ \theta_0+\theta_1 < 1}}
    \theta_0^{-\kappa+\kappa\eta+\kappa\xi}\theta_1^{-\kappa\eta}(1-(\theta_0+\theta_1))^{-\kappa\xi}
    = (1- \eta - \xi)^{-\kappa(1-\eta-\xi)}\eta^{-\kappa\eta}\xi^{-\kappa\xi}.
  }
  \begin{proof}
    For convenience we define
    \eq{f := \theta_0^{-\kappa+\kappa\eta+\kappa\xi}\theta_1^{-\kappa\eta}(1-(\theta_0+\theta_1))^{-\kappa\xi}.}
    To obtain the infimum we calculate
    \eq{\frac{\d f}{\d\theta_0} = f\cdot(\kappa(\eta+\xi-1)\theta_0^{-1} +\kappa\xi(1-\theta_0-\theta_1)^{-1}) \overset{!}{=} 0 \label{tI}}
    \eq{\frac{\d f}{\d\theta_1} = f\cdot(-\kappa\eta\theta_1^{-1} +\kappa\xi(1-\theta_0-\theta_1)^{-1}) \overset{!}{=} 0 \label{tII}}
    \eq{\eqref{tI}-\eqref{tII} \f \kappa(\eta+\xi-1)\theta_0^{-1} + \kappa\eta\theta_1^{-1} = 0
          \f \theta_1= \frac{\eta}{1-\eta-\xi}\theta_0.}
    Putting this into \eqref{tII} and solving the resulting equation yields
    \eq{\theta_0 = 1 - \eta - \xi, \quad \theta_1 = \eta.}
  \end{proof}
\end{thm}

\subsubsection{Unified notation for results and plots}
With $C_{\gamma,d}^{(a,b,c), \text{kf3}}$, $\gamma\in (a,c)$ we denote the upper bound that has been
achieved by Ky-Fan 3 power interpolation (kf3) with $p_0=a$, $p_1=b$, $p_2=c$, $L_{{p_i,d}} =
C_{p_i,d}^\text{lit}L_{p_i,d}^\text{cl}$ ($i=0,1$).

\subsubsection{Numerical Results}
In the following results $\eta$ has been chosen in the bound of \ref{etachoice}
so that $L_{p,1}$ becomes minimal. The resulting $\eta$ of the minimization can
be seen in Fig. \ref{plot:interp-kf-res3p-fact-eta}.
\plotinc{s1d3p_kf_total.tex}
{Numeric evaluation of the achieved constants for $d=1$, $p_0=\frac{1}{2}$, $p_1=1$, $p_2=\frac{3}{2}$,
$C_{\frac{1}{2},1}=2$, $C_{1,1}\le 1.456$, $C_{\frac{3}{2},1}=1$.}
{plot:interp-ky-fan-res3p-total}
\plotinc{s1d3p_kf_fact_eta.tex}
{Numeric evaluation of the optimal $\eta$ for $d=1$, $p_0=\frac{1}{2}$, $p_1=1$, $p_2=\frac{3}{2}$,
$C_{\frac{1}{2},1}=2$, $C_{1,1}\le 1.456$, $C_{\frac{3}{2},1}=1$.}
{plot:interp-kf-res3p-fact-eta}
\plotinc{s1d3p_kf_fact.tex}
{Numeric evaluation of the constituting factors for $d=1$, $p_0=\frac{1}{2}$, $p_1=1$, $p_2=\frac{3}{2}$,
$C_{\frac{1}{2},1}=2$, $C_{1,1}\le 1.456$, $C_{\frac{3}{2},1}=1$.}
{plot:interp-ky-fan-res3p-fact}

\paragraph{Comparison with 2 power interpolation results}\,
\plotinc{s1d3p_kf_total_comp.tex}
{Numeric comparison of the constituting factors for 3 and 2 point interpolation for $d=1$, $p_0=\frac{1}{2}$, ($p_1=1$), $p_2=\frac{3}{2}$,
$C_{\frac{1}{2},1}=2$, $C_{1,1}\le 1.456$, $C_{\frac{3}{2},1}=1$.}
{plot:interp-ky-fan-res3p-total-comp}
As seen in Fig. \ref{plot:interp-ky-fan-res3p-total-comp} the 3 power
interpolation is significantly worse than the two power interpolations.
The comparison of the constituting factors of our interpolations in Fig.
\ref{plot:interp-ky-fan-res3p-comp}, \ref{plot:interp-ky-fan-res3p-comp-2}
show that this is clearly due to the Ky-Fan induced $M$-Term
that gets drastically worse for 3 power interpolation.
\plotinc{s1d3p_kf_comp.tex}
{Numeric comparison of the constituting factors for 3 and 2 point interpolation for $d=1$, $p_0=\frac{1}{2}$, ($p_1=1$), $p_2=\frac{3}{2}$,
$C_{\frac{1}{2},1}=2$, $C_{1,1}\le 1.456$, $C_{\frac{3}{2},1}=1$.}
{plot:interp-ky-fan-res3p-comp}
\plotinc{s1d3p_kf_comp_2.tex}
{Numeric comparison of the constituting factors for 3 and 2 point interpolation for $d=1$, $p_0=\frac{1}{2}$, ($p_1=1$), $p_2=\frac{3}{2}$,
$C_{\frac{1}{2},1}=2$, $C_{1,1}\le 1.456$, $C_{\frac{3}{2},1}=1$.}
{plot:interp-ky-fan-res3p-comp-2}
