\section{Glazmanns lemma, Riesz means and the Rotfeld ineqality}
\label{sec:prelim_rf}
In this section we will follow \cite{weidlbook} in order 
to present and proof the Rotfel'd inequalities we need since these are
not as common in literature.
The author has taken the liberty to rewrite or omit parts or supplement details
to the theorems from \cite{weidlbook} as he saw fit, but this section
does not contain any significant new ideas by the author.
\begin{defn}[Spectral counting function]
  We define the spectral counting function $N(\,\cdot\,, A):\R \to \R\cup\m{\infty}$
  of a self-adjoint Operator $A$ by
  \eq{N(\mu, A) := \dim P_{(-\infty, \mu)}H.}
\end{defn}
For the rest of this section let $A$ be a self-adjoint lower semibounded operator.
\begin{thm}[Glazmanns lemma, \cite{weidlbook}]\label{glazmann}
  The counting function $N(\mu, A)$ satisfies
  \begin{enumerate}
    \item 
  \ileq{N(\mu, A) = \sup\mdefx{\dim F}{
    {F\subset D(A)\text{ subspace}}\,\land\,{\fa{0\neq u\in F}{(Au,u)<\mu\norm{u}^2}}}}
    and
    \item 
  \ileq{N(\mu, A) = \inf\mdefx{\dim F}{
    {F\subset H\text{ subspace}}\,\land\,{\fa{0\neq u\in F^\perp\cap D(A)}{(Au,u)\ge\mu\norm{u}^2}}}}.
  \end{enumerate}
\end{thm}
\begin{thm}[\cite{weidlbook}]\label{N0AB}
  Let $A$ and $B$ be self-adjoint lower semibounded operators with $\overline{D(A)\cap D(B)}=H$. Then
  \eq{N(0,A+B) \le N(0,A) + N(0,B).}
  \begin{proof}
    Let $F:= P_{(-\infty,0)}(A)H+P_{(-\infty,0)}(B)H$ and let $u\in F^\perp\cap D(A+B)$.
    Thus \eq{u\in D(A)\cap (P_{(-\infty,0)}(A)H)^\perp \f (Au,u)\ge 0} and
    \eq{u\in D(B)\cap (P_{(-\infty,0)}(B)H)^\perp \f (Bu,u)\ge 0.}
    Then \eq{((A+B)u,u) = (Au, u) + (Bu, u)\ge 0} which implies
    \eq{N(0,(A+B))\stackrel{\text{Thm. \ref{glazmann}}}{\le}\dim F
    \le \dim P_{(-\infty,0)}(A)H + \dim P_{(-\infty,0)}(B)H
    = N(0,A) + N(0,B).}
  \end{proof}
\end{thm}
\begin{thm}[\cite{weidlbook}]\label{boundconjglazmann}
  Let $T:H\to H$ be a bounded operator, then
  \eq{N(0,T^*AT)\le N(0,A).}
  \begin{proof}
    Let $L:=P_{(-\infty,0)}H$, $F=T^*L$.
    Let $u\in F^\perp \cap D(T^*AT)$, $v\in L$. Then \teq{(Tu,v)=(u,T^*v)\stackrel{u\in F^\perp, T^*v\in F}{=}0.}
    This implies $Tu\in L^\perp$ which by the properties of the Spectral measure of $A$ (Thm. \ref{pvmdop})
    implies $(ATu, Tu)\ge 0$. Thus we have
    \teq{\fa{u\in F^\perp \cap D(T*AT)} (T^*ATu,u)=(ATu,Tu)\ge 0.}
    By Glazmanns lemma (Thm. \ref{glazmann}) this implies $N(0,T^*AT)\le \dim F$.
    Since $\dim F \le \dim L = N(0, A)$ our result immeadiately follows.
  \end{proof}
\end{thm}
\begin{thm}[\cite{weidlbook}]\label{boundprojglazmann}
  Let $P=P^*=P^2$ be a projection, then
  \eq{N(\lambda, PAP\vert_{PH}) \le N(\lambda, A).}
  \begin{proof}\usefilleqproof
    \eq{N(\lambda,A) &= N(0, A - \lambda) \\
      &\ns{20}\stackrel{\text{Thm. \ref{boundconjglazmann}}}{\ge } N(0, P(A-\lambda)P)\\
      \interject{since}{P(A-\lambda)P = P(A-\lambda)P|_{PH}\oplus 0\text{ this is equal to}}
      &= N(0, (P(A-\lambda)P)\vert_{PH}) & \\
      \interject{since}{(P(A-\lambda)P)\vert_{PH}=(PAP-\lambda)\vert_{PH}\text{ this is equal to}}
      &= N(0,(PAP-\lambda)\vert_{PH})& \\
      &= N(\lambda,(PAP)\vert_{PH})
    }
  \end{proof}
\end{thm}
For the rest of this section let $A$ satisfy $\inf \sigma_{ess}(A)\ge 0$.
\begin{defn}\label{lambdandef}
  Let $0\le\kappa:=\inf\sigma_{ess}(A)$.
  Then there are at most countably many eigenvalues below $\kappa$, which can only
  accumulate at $\kappa$.
  Since $0\le \kappa$ this also means there are at most countably many eigenvalues
  below zero, which can only accumulate at zero.
  This allows us to define $\lambda_n(A)$ to be the monotone rising sequence of
  strictly negative eigenvalues of A, where each eigenvalue shall appear as
  often as its multiplicity.
  Here $\lambda_1(A)=\inf\sigma(A)$ is the lowest eigenvalue.
\end{defn}
\begin{defn}[Riesz means]\label{rieszmdef}
  Let $\lambda_n$ be defined as in Thm \ref{lambdandef}.
  Then we define the Riesz mean
  \eq{\Tr A_-^\gamma := \Tr((A_-)^\gamma) = \sum_n (-\lambda_n(A))^\gamma.}
\end{defn}
\begin{thm}[\cite{weidlbook}]\label{ewsumorth}
  Let $\lambda_n$ be defined as in Thm \ref{lambdandef} and let $\N\ni N\le N(0, A)$. Then
  \eq{\sum_{n=1}^N \lambda_n(A) = \inf\mdef{\sum_{n=1}^N(Au_n,u_n)}
  {(u_n)_{n=1}^N\subset D(A)\,\land\,\fa{1\le i,j\le N}(u_i,u_j)=\delta_{i,j}}.}
  \begin{proof}
    Let $N$ be arbitrary but fixed.
    \begin{itemize}
      \item[``$\ge$'']
        Since $\lambda_n(A)$ are discrete eigenvalues there exists
        an orthogonal basis $(v_n)_{n=1}^N$ for the eigenspaces associated with them,
        i.e. $(v_n,v_m)=\delta_{i,j}$ and $Av_n = \lambda_n(A)v_n$. Then
        \eq{\inf\mdef{\sum_{n=1}^N(Au_n,u_n)}{\dots}
        \le \sum_{n=1}^N (Av_n,v_n) = \sum_{n=1}^N \lambda_n(A).}
      \item[``$\le$'']
        Let $(u_n)_{n=1}^N$, $(u_i,u_j)=\delta_{i,j}$ be arbitrary but fixed.
        Then $P:=\sum_{n=1}^N(\cdot,u_n)u_n$ is an orthogonal projection which
        by Thm. \ref{boundprojglazmann} implies
        \teq{\fa{\lambda<0}N(\lambda, (PAP)\vert_{PH})\le N(\lambda,A).}
        Let $1\le j \le n$. Then \teq{\lambda_j(A)\le \lambda_j((PAP)\vert_{PH})} since
        $\lambda_j(A) > \lambda_j((PAP)\vert_{PH})$ would contradict the last inequality
        with \eq{\lambda = \frac{\lambda_j((PAP)\vert_{PH})+\lambda_j(A)}{2}}
        since then $N(\lambda, A) = j-1$, $N(\lambda,(PAP)\vert_{PH})=j$. This implies
        \eq{\sum_{n=1}^N \lambda_n(A) &\le \sum_{n=1}^N \lambda_n((PAP)\vert_{PH})\\
          \interject{and since}{\Tr\text{ is independent of base this equals}}
          &=\Tr((PAP)\vert_{PH})\\
          &= \sum_{n=1}^N(PAPu_n,u_n) = \sum_{n=1}^N(Au_n,Pu_n) = \sum_{n=1}^N(Au_n,u_n).}
    \end{itemize}
  \end{proof}
\end{thm}
\begin{thm}[\cite{weidlbook}]\label{ewsumorthtr}
  It holds that
  \eq{-\Tr A_- = \inf\mdef{\sum_{n=1}^N(Au_n,u_n)}
  {N\in\N,(u_n)_{n=1}^N\subset D(A)\land\fa{1\le i,j\le N}(u_i,u_j)=\delta_{i,j}}}
  \begin{proof}
    If $N(0,A)=\infty$ then taking the limit $N\to\infty$ in Thm.
    \ref{ewsumorth} immediately gives the result.
    If $M := N(0,A) < \infty$ then Thm. \ref{ewsumorth} gives us
    \eq{
      -\Tr A_- &= \sum_{n=1}^M \lambda_n(A) \\
      &= \inf\mdef{\sum_{n=1}^M(Au_n,u_n)}
      {(u_n)_{n=1}^M\subset D(A)\,\land\,\fa{1\le i,j\le N}(u_i,u_j)=\delta_{i,j}}\\
      &\ge \inf\mdef{\sum_{n=1}^N(Au_n,u_n)}
      {N\in\N,(u_n)_{n=1}^N\subset D(A)\land\fa{1\le i,j\le N}(u_i,u_j)=\delta_{i,j}}.
    }
    However, since $M = \dim P_{(-\infty, 0)}H$ and the $u_n$ are orthonormal
    at most $M$ summands of the last sum can be strictly negative.
    Thus the infimum is taken for $N=M$ and the last inequality is in fact also
    an equality.
  \end{proof}
\end{thm}
\begin{cor}[\cite{weidlbook}]\label{TrAB}
  It holds that
  \eq{\Tr (A+B)_- \le \Tr A_- + \Tr B_-.}
  \begin{proof}
    Let $N\in\N$, $(u_n)_{n=1}^N$, $(u_i,u_j)=\delta_{i,j}$. Then
    \eq{-\Tr (A+B)_- \xleftarrow[\text{Thm. \ref{ewsumorthtr}}]{\inf \N, (u_n)}
    \sum_{n=1}^N ((A+B)u_n,u_n) = \sum_{n=1}^N (Au_n,u_n) + \sum_{n=1}^N (Bu_n,u_n)
    \ge - \Tr A_- - \Tr B_-.}
  \end{proof}
\end{cor}
\begin{thm}[\cite{weidlbook}]\label{Trgint}
  \begin{enumerate}
    \item For $\gamma > 0$ it holds that
      \eq{\Tr A_-^\gamma = \gamma\int_0^\infty N(-\kappa, A)\kappa^{\gamma - 1} \d\kappa \label{TrgintN}.}
    \item For $\gamma > 1$ it holds that
      \eq{\Tr A_-^\gamma = \gamma(\gamma - 1)\int_0^\infty \Tr(A+\kappa)_- \kappa^{\gamma - 2} \d\kappa \label{TrgintTr}.}
  \end{enumerate}
  \begin{proof}
    \begin{enumerate}
      \item It is easily seen that
          \eq{\fa{\kappa\ge 0} N(-\kappa, A) = \dim P_{(-\infty, -\kappa)}H
          = \left(\sum_{n=1}^\infty \chi((0,-\lambda_n(A)))\right)(\kappa).}
          Using this equality we can prove the desired result in a small calculation:
          \eq{\int_0^\infty \gamma N(-\kappa, A)\kappa^{\gamma-1} \d \kappa
          &= \int_0^\infty \gamma \sum_{n=1}^\infty \chi((0,-\lambda_n(A))) \kappa^{\gamma-1} \d \kappa\\
          &= \sum_{n=1}^\infty \int_0^\infty \gamma  \chi((0,-\lambda_n(A))) \kappa^{\gamma-1} \d \kappa\\
          &= \sum_{n=1}^\infty \int_0^{-\lambda_n(A)} \gamma \kappa^{\gamma-1} \d \kappa
          = \sum_{n=1}^\infty [\kappa^\gamma]_0^{-\lambda_n(A)}\\
          &= \sum_{n=1}^\infty (-\lambda_n(A))^\gamma
          = \Tr A_-^\gamma.}
        \item We have $\lambda_n(A+\kappa) = \lambda_n(A)+\kappa$.
          But all eigenvalues $\lambda_n(A+\kappa)\ge 0$ need to be ``dropped''
          for $\Tr(A+\kappa)_-$, so we end up with
          \eq{
            \gamma(\gamma - 1)\int_0^\infty \Tr(A+\kappa)_- \kappa^{\gamma - 2} \d\kappa
            &= \gamma(\gamma - 1)\int_0^\infty \sum_{n=1}^\infty-(\lambda_n(A)+\kappa)\chi([0,-\lambda_n(A)]) \kappa^{\gamma - 2} \d\kappa\\
            &= \gamma(\gamma - 1)\sum_{n=1}^\infty \int_0^{-\lambda_n(A)} -(\lambda_n(A)+\kappa) \kappa^{\gamma - 2} \d\kappa\\
            &= \gamma(\gamma - 1)\sum_{n=1}^\infty \left[-\lambda_n(A)\frac{1}{\gamma-1}\kappa^{\gamma-1}
                -\frac{1}{\gamma}\kappa^\gamma\right]_0^{-\lambda_n(A)}\\
            &= \gamma(\gamma - 1)\sum_{n=1}^\infty (-\lambda_n(A))^\gamma\cdot\left(\frac{1}{\gamma-1}-\frac{1}{\gamma}\right)\\
            &= \sum_{n=1}^\infty (-\lambda_n(A))^\gamma
            = \Tr A_-^\gamma.
          }
    \end{enumerate}
  \end{proof}
\end{thm}
\begin{thm}[\cite{weidlbook}]
  Let $T$ be bounded and $\gamma>0$. Then
  \eq{\Tr(T^*AT)_-^\gamma \le \norm{T}^{2\gamma} \Tr A_-^\gamma \label{TATTr}.}
  \begin{proof}
    We have
    \eq{\frac{T^*T}{\norm{T}^2}\le I \f T^*AT+\kappa\ge T^*(A+\norm{T}^{-2}\kappa)T}
    \eq{\f N(-\kappa, T^*AT) = N(0, T^*AT+\kappa) &\le N(0, T^*(A+\norm{T}^{-2}\kappa)T) \\
      \overset{\text{Thm. \ref{boundconjglazmann}}}&{\le} N(0, A+\norm{T}^{-2}\kappa)
      = N(-\norm{T}^{-2}\kappa, A)\label{pNnormT}}
    With this inequality we can prove the result in a small calculation:
    \eq{\Tr(T^*AT)_-^\gamma
      \overset{\eqref{TrgintN}}&{=} \gamma\int_0^\infty N(-\kappa, T^*AT)\kappa^{\gamma - 1} \d\kappa\\
      \overset{\eqref{pNnormT}}&{\le} \gamma\int_0^\infty N(-\norm{T}^{-2}\kappa, A)\kappa^{\gamma - 1} \d\kappa
      \quad \text{Subst. $\bar{\kappa}=\norm{T}^{-2}\kappa$}\\
      &= \norm{T}^{2\gamma} \gamma\int_0^\infty N(-\bar{\kappa}, A)\bar{\kappa}^{\gamma - 1} \d\bar{\kappa}\\
      \overset{\eqref{TrgintN}}&{=} \norm{T}^{2\gamma} \Tr A_-^\gamma.
    }
  \end{proof}
\end{thm}
\begin{cor}
  Let $P$ be a projection, then
  \eq{\Tr(PAP)_-^\gamma \le \Tr A_-^\gamma \label{PAPTr}.}
  \begin{proof}
    Use \eqref{TATTr} with $\norm{P}=1$.
  \end{proof}
\end{cor}
\begin{thm}[\cite{weidlbook}]\label{roneproj}
  Let $B$ be an operator of rank one ($\dim\Im(B)=1$), and $I=(a,b)$ with $a,b\in\R$, then
  \eq{\dim P_I(A)H-1\le\dim P_I(A+B)H\le\dim P_I(A)H+1.}
  \begin{proof}
\begin{enumerate}
  \item
    $P_I(A+B)H\le\dim P_I(A)H+1$: Contrary assumption:
    \eq{
      \dim P_I(A+B)H>\dim P_I(A)H+1 &\f P_I(A+B)H - \dim P_I(A)H - 1 > 0 \\
      &\f \ex{x\neq 0}x\in P_I(A+B)H \cap (P_I(A)H)^\perp\cap \ker B.}
    For this choice of $x$ it then follows that
    \eq{x\in P_I(A+B)H \f \ex{y\in H} P_I(A+B)y = x}
    which we use to derive
    \eq{(P_{\R\sm I}(A+B)x, x) &= (P_{\R\sm I}(A+B)x, P_I(A+B)y)\\
      &= (P_I(A+B)P_{\R\sm I}(A+B)x,y) = (P_\lm x, y) = 0.
      \label{roneprojp1}}
    Let $\alpha = \inf I + \abs{I}/2$. Then we have
    \eq{\norm{(A+B-\alpha)x}^2 &= ((A+B-\alpha)^2x,x)\\
      &= \int_\R (\lambda - \alpha)^2 \d (P_\lambda(A+B)x,x)\\
      \overset{\eqref{roneprojp1}}&{=} \int_I (\lambda - \alpha)^2 \d (P_\lambda(A+B)x,x)
      < \frac{\abs{I}^2}{4}\norm{x}^2.}
    But since $x\in (P_I(A)H)^\perp \f (P_I(A)x,x)= 0$ we also have
    \eq{\norm{(A-\alpha)x}^2 =
      \int_{\R\sm I} (\lambda - \alpha)^2 \d (P_\lambda(A)x,x) \ge \frac{\abs{I}^2}{4}\norm{x}^2}
    which contradicts the previous equation since $x\in\ker B\f (A + B - \alpha)x = (A - \alpha)x$.
  \item
    $\dim P_I(A)H-1\le \dim P_I(A+B)H$: Use the previously proven inequality with $A' = A + B$, $B' = - B$.
\end{enumerate}
  \end{proof}
\end{thm}
\begin{cor}[\cite{weidlbook}]\label{roneewcor}
  Let $B$ be a negative ($B\le 0$) rank one operator. Then
  \eq{\lambda_1(A+B)\le \lambda_1(A) \le \lambda_2(A+B) \le \lambda_2(A) \le \dots .}
  \begin{proof}
    \begin{enumerate}
      \item $B\le 0 \f \lambda_n(A+B)\le \lambda_n(A)$.
      \item
        To prove $\lambda_n(A)\le \lambda_{n+1}(A+B)$
        let us make the opposite assumption $\ex{n\in\N}\lambda_n(A) > \lambda_{n+1}(A+B)$.\\
        Let $I:= (\lambda_1(A+B) - \varepsilon, \lambda_n(A))$ for arbitrary but fixed $\varepsilon>0$.
        Then \eq{[\lambda_1(A+B),\lambda_{n+1}(A+B)]\subset I \f \dim P_I(A+B)H \ge n+1} and \eq{\dim P_I(A)H < n.}
        But form Thm. \ref{roneproj} we gain $\dim P_I(A+B)H \le \dim P_I(A)H+1$.
        Then we have \eq{n+1\le P_I(A+B)H\le P_I(A)H + 1 < n + 1\text{\lightning}.}
    \end{enumerate}
  \end{proof}
\end{cor}
\begin{thm}[\cite{weidlbook}]\label{intmeasE}
  Let $0<\gamma<1$ and $E\subset [0,\infty)$, $\abs{E}<\infty$. Then
  \eq{\int_Et^{\gamma-1}\d t\le\int_0^{\abs{E}}t^{\gamma-1}\d t = \frac{\abs{E}^\gamma}{\gamma}.}
  \begin{proof}
    We have
    \eq{t^{\gamma-1} = -(\gamma-1)\left[\frac{1}{\gamma-1}s^{\gamma-1}\right]^\infty_t
        = -(\gamma-1)\int_t^\infty s^{\gamma-2} \d s
        = (1-\gamma)\int_0^\infty\chi((0,s))(t)s^{\gamma-2}\d s.}
    We can directly use this:
    \eq{\int_E t^{\gamma-1} \d t &= \int_0^\infty \chi(E) t^{\gamma-1}\\
    &= (1-\gamma)\int_0^\infty \int_0^\infty \chi(E)\chi((0,s))(t)s^{\gamma-2}\d s \d t \\
    &= (1-\gamma)\int_0^\infty \int_0^\infty \chi(E)\chi((0,s))(t)\d t s^{\gamma-2}\d s  \\
    &= (1-\gamma)\int_0^\infty \abs{E\cap (0,s)} s^{\gamma-2}\d s  \\
    &\le (1-\gamma)\int_0^\infty \min\m{\abs{E}, s} s^{\gamma-2}\d s  \\
    &= (1-\gamma) \int_0^{\abs{E}}s^{\gamma-1}\d s
       + (1-\gamma) \int_{\abs{E}}^\infty \abs{E} s^{\gamma-2}\d s  \\
    &= (1-\gamma) \frac{\abs{E}^\gamma}{\gamma}
       - (1-\gamma) \frac{\abs{E}^\gamma}{\gamma-1} = \frac{\abs{E}^\gamma}{\gamma}.
    }
  \end{proof}
\end{thm}
The following inequalities are named after S. Rotfel'd who proved them for the
$\gamma<1$ case in \cite{rotfeld69}.
However, in the following we will still follow \cite{weidlbook} since it
provides an extended and optimised version of the inequality and proof:
\begin{thm}[Rotfeld-Inequality, \cite{weidlbook}]\label{rfineq}
  Let $A$, $B$ be self-adjoint operators which are semi-bound from below.
  Then for $\gamma > 0$ and $\vartheta\in(0,1)$ it holds that
  \eq{\Tr(A+B)^\gamma_- \le \vartheta^{-\gamma}\Tr(A)^\gamma_- + (1-\vartheta)^{-\gamma}\Tr(B)^\gamma_-.}
  For $\gamma\in(0,1)$ it holds that
  \eq{\Tr(A+B)^\gamma_- \le \Tr(A)^\gamma_- + \Tr(B)^\gamma_-.}
  For $\gamma > 1$ it holds that
  \eq{\Tr(A+B)^\gamma_- \le \vartheta^{-\gamma + 1}\Tr(A)^\gamma_- + (1-\vartheta)^{-\gamma + 1}\Tr(B)^\gamma_-.}
  By taking the limit to $\gamma=1$ in either one of the equations one yields the same estimate
  \eq{\Tr(A+B)_- \le \Tr(A)_- + \Tr(B)_-.}
  So we can ``unify'' these to a best estimate of
  \eq{\Tr(A+B)^\gamma_- \le a(\gamma,\vartheta)\Tr(A)^\gamma_- + b(\gamma,\vartheta)\Tr(B)^\gamma_-}
  with \eq{
    a(\gamma,\vartheta) = \begin{cases} 1 & \gamma \le 1 \\ \vartheta^{-\gamma + 1} & \gamma > 1\end{cases}
    ,\qquad
    b(\gamma,\vartheta) = \begin{cases} 1 & \gamma \le 1 \\ (1-\vartheta)^{-\gamma + 1} & \gamma > 1\end{cases}
  }
  and $\vartheta\in(0,1)$.
  \begin{proof}
    \begin{enumerate}
      \item
        Inequality for $\gamma > 0$: Let $\kappa>0$.
        Then
        \eq{N(-\kappa, A+B) &= N(0, (A + \vartheta\kappa) + (B + (1 - \vartheta)\kappa)) \\
          \overset{\text{Thm. \ref{N0AB}}}&{\le} N(0, A + \vartheta\kappa) + N(0, B + (1 - \vartheta)\kappa) \\
          &= N(-\vartheta\kappa, A) + N(-(1 - \vartheta)\kappa, B).
          \label{rfproof1Neq}
        }
        With this inequality and \eqref{TrgintN} we can prove the desired inequality:
        \eq{\Tr(A+B)_-^\gamma
          \overset{\eqref{TrgintN}}&{=} \gamma \int_0^\infty N(-\kappa, (A+B))\kappa^{\gamma-1}\d \kappa \\
          \overset{\eqref{rfproof1Neq}}&{\le} \gamma \int_0^\infty N(-\vartheta\kappa, A)\kappa^{\gamma-1}\d \kappa
          + \gamma \int_0^\infty N(-(1 - \vartheta)\kappa, B)\kappa^{\gamma-1}\d \kappa \\
          \overset{\text{lin. subst., }\eqref{TrgintN}}&{=} \vartheta^{-\gamma}\Tr(A)^\gamma_- + (1-\vartheta)^{-\gamma}\Tr(B)^\gamma_-.
        }
      \item
        Inequality for $0 < \gamma < 1$:
        \begin{enumerate}
          \item In case $B$ is rank one we have by Cor. \ref{roneewcor}
            \eq{\lambda_1(A+B)\le \lambda_1(A) \le \lambda_2(A+B) \le \lambda_2(A) \le \dots \,.}
            This allows us to define a disjoint set of intervals
            $I_n:=(-\lambda_n(A), -\lambda_n(A+B))$ and their disjoint union $E:=\sqcup_{n\in\N} I_n$.
            Then \eq{\abs{E} = \sum_{n=1}^\infty \abs{I_n} = \sum_{n=1}^\infty -\lambda_n(A+B) + \lambda_n(A)
              = \Tr(A+B)_- + \Tr(A)_- \stackrel{\text{Cor. \ref{TrAB}}}{\le} \Tr(B)_- \qquad\label{Emeasabs}}
            and
            \eq{\Tr(A+B)_-^\gamma &= \sum_{n=1}^\infty -\lambda_n(A+B)^\gamma\\
                &= \sum_{n=1}^\infty \gamma \int_0^{-\lambda_n(A+B)} t^{\gamma -1}\\
                &= \sum_{n=1}^\infty \gamma \int_0^{-\lambda_n(A)} t^{\gamma -1}
                  + \sum_{n=1}^\infty \gamma \int_{-\lambda_n(A)}^{-\lambda_n(A+B)} t^{\gamma -1}\\
                &= \Tr(A)_-^\gamma + \sum_{n=1}^\infty \gamma \int_{I_n} t^{\gamma -1}\\
                &= \Tr(A)_-^\gamma + \gamma \int_E t^{\gamma -1}\\
                \overset{\text{Thm. \ref{intmeasE}}}&{\le} \Tr(A)_-^\gamma + \abs{E}^\gamma\\
                \overset{\eqref{Emeasabs}}&{\le} \Tr(A)_-^\gamma + \Tr(B)_-^\gamma .}
          \item In case $B$ is of finite rank we have
            \eq{\Tr(A+B)_-^\gamma &= \Tr\left(A+ \sum_{n=1}^N P_\m{\lambda_n(B)}B\right)_-^\gamma\\
                \interject{since}{\dim\Im P_\m{\lambda_n(B)}B = 1
                \text{ we can use the rank one inequality that we have just proven}}
                &\le
                  \Tr\left(A+ \sum_{n=1}^{N-1} P_\m{\lambda_n(B)}B\right)_-^\gamma
                  + \Tr(P_\m{\lambda_N(B)}B)_-^\gamma\\
                &\le \dots \le \Tr(A)_-^\gamma + \sum_{n=1}^N \Tr(P_\m{\lambda_n(B)}B)_-^\gamma\\
                &= \Tr(A)_-^\gamma + \sum_{n=1}^N (-\lambda_n(B))^\gamma\\
                &= \Tr(A)_-^\gamma + \Tr(B)_-^\gamma.}
          \item In case of arbitrary $B$ we define $P_\kappa := P_{(-\infty, -\kappa)}$ for $\kappa > 0$. Then
            \eq{\Tr(A+B)_-^\gamma \xleftarrow{\kappa\to 0} \sum_{\lambda_n(A+B) < -\kappa} (-\lambda_n(A+B))^\gamma
                &=\Tr(P_\kappa(A+B)P_\kappa)_-^\gamma \\
                &=\Tr(P_\kappa A P_\kappa + P_\kappa B P_\kappa)_-^\gamma \\
                \interject{since}{\dim\Im P_\kappa B \le \infty
                \text{ we can use the finite rank inequality that we have just proven}}
                &\le \Tr(P_\kappa A P_\kappa)_-^\gamma + \Tr(P_\kappa B P_\kappa)_-^\gamma \\
                \overset{\eqref{PAPTr}}&{\le} \Tr(A)_-^\gamma + \Tr(B)_-^\gamma.
                }
        \end{enumerate}
      \item
        Inequality for $\gamma > 1$: Let $\kappa>0$. Then
        \eq{\Tr(A+B+\kappa)_- &= \Tr((A + \vartheta\kappa) + (B + (1 - \vartheta)\kappa))_- \\
          \overset{\text{Thm. \ref{TrAB}}}&{\le}\quad \Tr(A + \vartheta\kappa)_- + \Tr(B + (1 - \vartheta)\kappa)_-.
          \label{rfproof3Treq}}
        With this inequality and \eqref{TrgintTr} we can prove the desired inequality:
        \eq{\Tr(A+B)_-^\gamma
          \overset{\eqref{TrgintTr}}&{=} \gamma(\gamma - 1) \int_0^\infty \Tr(-\kappa, (A+B))_-\kappa^{\gamma-2}\d \kappa \\
          \overset{\eqref{rfproof3Treq}}&{\le} \gamma(\gamma - 1) \int_0^\infty \Tr(-\vartheta\kappa, A)_-\kappa^{\gamma-2}\d \kappa
          + \gamma(\gamma - 1) \int_0^\infty \Tr(-(1 - \vartheta)\kappa, B)_-\kappa^{\gamma-2}\d \kappa \\
          \overset{\text{lin. subst., }\eqref{TrgintTr}}&{=}
          \vartheta^{-\gamma+1}\Tr(A)^\gamma_- + (1-\vartheta)^{-\gamma+1}\Tr(B)^\gamma_-.
          }
    \end{enumerate}
  \end{proof}
\end{thm}
