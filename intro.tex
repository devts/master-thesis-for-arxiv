\chapter{Introduction}
\input{notation.tex}
\section{Subject of this work}
From physics the question of the stability of quantum mechanical matter has
sprung up and the first proof of this came by Dyson and Lenard in \cite{dyson67}.
Lieb and Thirring went on to prove this result with constants in the right
order of magnitude in 1975 in \cite{lt1975}.
This proof utilized an inequality that was soon generalized by Lieb and
Thirring in 1976 in \cite{lt1976} to the family of inequalities that is now
known under the name of Lieb-Thirring and is subject of much mathematical
research.
The Lieb-Thirring inequality concerns the ratio between moments of the
negative eigenvalues $E_j(H)$ of the Schrödinger operator $H = -\laplace + V$
and norm of the negative part of the potential $V_-(x):=\max(-V(x),0)$:
\eq{\sum_{n}\abs{E_n(H)}^\gamma \le L_{\gamma, d}\int_{\R^d} V_-^{\gamma+\frac{d}{2}}\d x.}
Here, we define the Lieb-Thirring constant $L_{\gamma, d}$ as the minimal
constant in the Lieb Thirring inequality.
This constant exists and is finite if and only if
\eq{\mcases{\text{$\gamma\ge\frac{1}{2}$ in $d=1$}\\
    \text{$\gamma > 0$ in $d=2$}\\
    \text{$\gamma \ge 0$ in $d\ge 3$}}}
as proven in \cite{lt1976, Lmonotonic, laptevweidlop, liebL0d, weidlinterpolation}.
For $V\in L_{\gamma+\frac{d}{2}}$ with $\gamma$ as in \eqref{gammalti}
it holds that in the semi-classical limit (``$\hbar\to 0$'')
\eq{\lim_{\alpha\to\infty} \alpha^{-\gamma+\frac{d}{2}}\Tr(-\laplace + \alpha V)_-^\gamma
    = L_{\gamma, d}^\text{cl}\int_{\R^d} V_-^{\gamma+\frac{d}{2}}\d x}
with the classical constant
\eq{L_{\gamma, d}^\text{cl} = (4\pi)^{-\frac{d}{2}}\frac{\Gamma(\gamma + 1)}{\Gamma(\gamma + 1 + \frac{d}{2})}.}
This constant not only immediately obtains us
\eq{L_{\gamma, d} \ge L_{\gamma, d}^\text{cl}}
but is also vital for the following important monotonicity property of the Lieb-Thirring constants:
\eq{\sds\frac{L_{\gamma, d}}{L_{\gamma, d}^\text{cl}} \text{ is monotonically decreasing in $\gamma$ for all $d\ge 1$},}
which was fist proven by Aizenmann and Lieb in \cite{Lmonotonic}.
Since thanks to the monotonicity property most problems are
more natural to express in ``units'' of $L_{\gamma,d}^\text{cl}$
we introduce
\eq{C_{\gamma,d} := L_{\gamma,d} / L_{\gamma,d}^\text{cl}.}
The question of which value the Lieb-Thirring constants have exactly has only been solved in the case
$\gamma\ge\frac{3}{2}$, $d\in\N$ with the result
$C_{\gamma,d} = 1$
and in the case $\gamma=\frac{1}{2}$, $d=1$ with the result
$C_{\frac{1}{2},1} = 2$.
In other cases we have the following best known upper bounds in literature \cite{liebL0d, hkrvL0d, L121sharp, fhrL11} as
\eq{C_{\gamma,1}^\text{lit}
    :=\mcases{2 & \gamma\in [\sfrac{1}{2},1)\\1.456 & \gamma\in [1,\sfrac{3}{2})\\ 1 & \gamma\in [\sfrac{3}{2},\infty)}
\qquad\qquad
    C_{\gamma,2}^\text{lit}
    :=\mcases{2.912 & \gamma\in [\sfrac{1}{2},1))\\ 1.456 & \gamma\in [1,\sfrac{3}{2})\\ 1 & \gamma\in [\sfrac{3}{2},\infty)}}
\eq{C_{\gamma,3}^\text{lit}:=\mcases{6.86924 & \gamma\in [0,\sfrac{1}{2})\\ 2.912 & \gamma\in [\sfrac{1}{2},1)\\
     1.456 & \gamma\in [1,\sfrac{3}{2})\\ 1 & \gamma\in [\sfrac{3}{2},\infty)}
\qquad\qquad
    C_{\gamma,4}^\text{lit}:=\mcases{6.03398 & \gamma\in [0,\sfrac{1}{2})\\ 2.912 & \gamma\in [\sfrac{1}{2},1)\\
     1.456 & \gamma\in [1,\sfrac{3}{2})\\ 1 & \gamma\in [\sfrac{3}{2},\infty)}}
Since these upper bounds are discontinuous it stands to reason, that these can
be partially improved and made continuous with the use of interpolation theory,
as was done by Weidl in \cite{weidlinterpolation} for $d=1$ for the bounds
known at the time of writing which are worse than those available now.
The Interpolation done by Weidl uses the Ky-Fan inequality
\eq{\abs{E_{n+m-1}(A+B)}\le\abs{E_n(A)}+\abs{E_m(B)}}
to obtain its interpolation results. In this work we will present updated
numbers for this approach based on the better bounds available from recent
literature and present a new approach for interpolation based on the Rotfel'd
inequality
\eq{\Tr(A+B)^\gamma_- \le a(\gamma,\vartheta)\Tr(A)^\gamma_- + b(\gamma,\vartheta)\Tr(B)^\gamma_-}
with $\vartheta\in(0,1)$ and \eq{
  a(\gamma,\vartheta) = \begin{cases} 1 & \gamma \le 1 \\ \vartheta^{-\gamma + 1} & \gamma > 1\end{cases}
  ,\qquad
  b(\gamma,\vartheta) = \begin{cases} 1 & \gamma \le 1 \\ (1-\vartheta)^{-\gamma + 1} & \gamma > 1\end{cases}.
}
instead of the Ky-Fan inequality that leads to better results.
Furthermore we will also present interpolation results in dimensions two to four.

\section{Structure of this work}
This work is my master thesis with some corrections and format changes.
Sections \ref{sec:prelim_bst} to \ref{sec:lti_theory} are mainly just a summary of
literature that my be skipped over if the reader is already familiar with the
topics of these sections.
\subsection*{Chapter \ref{ch:prelim}}
\paragraph*{Section \ref{sec:prelim_bst}}
This chapter contains a small recap of the basic definitions of spectral theory,
both as a quick remainder of the relevant basics and to define our notation in
these matters.
\paragraph*{Section \ref{sec:prelim_rf}}
This chapter contains a proof of the Rotfel'd inequality we need for our
interpolation results and a summary of previously establisted results in
spectral theory which are needed for the proof.

\subsection*{Chapter \ref{ch:lti}}
\paragraph*{Section \ref{sec:lti_theory}}
This section contains a summary of previously established results in the theory of
Lieb-Thirring equations.
\paragraph*{Section \ref{sec:lti_scoulomb}}
Here we look at the ratio beetween eigenvalues and the norm of the potential
not for arbitrary functions $V$, but for shifted Coulomb potentials
$V=-\abs{x}^{-1}+\lambda$ specifically.

\subsection*{Chapter \ref{ch:interpolation}}
This chapter contains the main content, that is the application of
interpolation theory to achieve upper bounds on the Lieb-Thirring constants.
\paragraph*{Section \ref{intp}}
Here we interpolate between two choices of powers $L_{p_0, d}$, $L_{p_1, d}$ in
the Lieb-Thirring equality to obtain bounds on $L_{\gamma, d}$
for intermediate exponents $\gamma$, $p_0<\gamma<p_1$.
\paragraph{Subsection \ref{intp2gen}}
  In this subsection we present the basics of the interpolation theory that was
  applied to Lieb-Thirring inequalities (LTI) by Weidl in 1996
  \cite{weidlinterpolation}.
\paragraph{Subsection \ref{intp2kf}}
  In this subsection we present the original interpolation technique of Weidl
  that is using a Ky-Fan inequality and slightly extend it to cover dimensions
  greater than one.
  Since the theory of Lieb-Thirring inequalities has advanced a lot and there
  are now better results available as a basis for interpolation since the time
  of Weidl's 1996 paper, we provide updated numbers for the Ky-Fan based
  interpolation approach of Weidl.
\paragraph{Subsection \ref{intp2rf}}
  In this subsection we present a new version of interpolation for
  Lieb-Thirring inequalities based on Rotfel'd inequalities instead of the
  Ky-Fan inequality used by Weidl that manages to achieve better constants.
  Furthermore we also present interpolation results in dimensions one to four.
\paragraph*{Section \ref{intp3}}
  We extend the results of the previous section to interpolation between three
  different powers $L_{p_0, d}$, $L_{p_1, d}$, $L_{p_2, d}$ in
  the Lieb-Thirring equality to obtain bounds on $L_{\gamma, d}$
  for intermediate exponents $\gamma$, $p_0<\gamma<p_2$.
