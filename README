Title: New interpolation results for Lieb-Thirring inequalities
Authors: Timo Schmetzer

Abstract:
Interpolation theory was first applied to Lieb-Thirring inequalities (LTI) by
Weidl in 1996 using a Ky-Fan inequality. However since then the theory of
Lieb-Thirring inequalities has advanced a lot and there are now better results
available as a basis for interpolation. We provide both updated numbers for the
Ky-Fan based interpolation approach of Weidl with the new bounds available at
the time of writing and we present a new version of interpolation for
Lieb-Thirring inequalities based on Rotfel'd inequalites instead of the Ky-Fan
inequality used by Weidl that manages to achieve better constants. We also
extend these interpolation methods to interpolation in three different powers.

Comments: Master thesis with corrections.
Subjects: Spectral Theory (math.SP); Mathematical Physics (math-ph)
MSC-class: Primary 81Q10

Finished pdfs can be found under:
https://codeberg.org/devts/master-thesis-for-arxiv/releases
Direct pdf links:
[v1] https://codeberg.org/attachments/a1a0d333-e7fb-400f-a7d3-a0345161a8f8

If you would like to see this on arxiv and you are eligible for endorsement to
math.SP you can endorse this work for upload here:
https://arxiv.org/auth/endorse?x=Z34WS7
